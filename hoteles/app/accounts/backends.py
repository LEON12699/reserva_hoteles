from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.db.models import Q


class EmailOrUsernameModelBackend(ModelBackend):
    
    def authenticate(self, request,username=None, password=None,**kwargs):
        auth_type = settings.AUTH_AUTHENTICATION_TYPE
        UserModel = get_user_model()
        if auth_type == 'username':
            return super().authenticate(username, password)
        try:
            if auth_type == 'both':
                user = UserModel.objects.get(
                    Q(username__iexact=username)|Q(email__iexact=username)
                )
            else:
                user = UserModel.objects.get(email__iexact=username)
            UserModel().set_password(password)
            if user.check_password(password):
                return user
        except UserModel.DoesNotExist:
            return None