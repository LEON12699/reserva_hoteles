from app.modelos.models import usuario
from django import forms
from django.contrib.auth.models import User
from app.modelos.models import usuario


from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import *

class CuentaForm(forms.ModelForm):
    class Meta:
        model = User
        fields=("username","password","email")
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.fields['email'].required = True;
        self.fields['password'].widget.input_type="password"

    def clean_email(self):
        data = self.cleaned_data["email"]
        if(User.objects.filter(email=data).exists()):
            raise forms.ValidationError("el email ya esta registrado")
        return data
        


class usuarioForm(forms.ModelForm):
    class Meta:
        model = usuario
        fields=("nombre","apellidos","pais","telefono")


class UsuarioFormEdit(forms.ModelForm):

    username= forms.CharField(label="Nombre usuario",max_length=100)
    tel_cod= forms.CharField(max_length=4,required=False)

    class Meta:
        model = usuario
        fields=("nombre","apellidos","pais","telefono","contactos","imagen","username")

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.fields['contactos'].widget.attrs['readonly']=True
        self.fields['tel_cod'].disabled=True
        #agregar imagen si hay tiempo si no deja alli
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(#Row(HTML("<img  src='https://upload.wikimedia.org/wikipedia/commons/5/51/No_imagen_disponible.gif' width=100 heigth=100></img>"),Div("username",css_class="col")),
        "username",
        Row(Div("nombre",css_class="col"),Div("apellidos",css_class="col")),
        Row(Div("contactos",css_class="col-5"),Div("tel_cod",css_class="col-2"),Div("telefono",css_class="col-5")),
        "pais")

            
class FormularioLogin(forms.Form):
    username=forms.CharField(required=True)
    password=forms.CharField(widget = forms.PasswordInput())
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.form_tag=False
        self.helper.layout = Layout(Field("username",css_class="form-control mr-sm-1",placeholder="usuario o email",aria_label="usuario"),
        Field("password",css_class="form-control mr-sm-1",placeholder="password"  ,aria_label="password")
          )