from django.shortcuts import render


from django.forms import formset_factory
# Create your views here.
from app.hotel.form import HotelForm
from django.contrib import messages
from .forms import *
from app.modelos.models import usuario,Country,City,Region,Servicios
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required

from app.usuario.forms import UsuarioFormEdit

import datetime

from django.http import HttpResponseRedirect
from django.urls import reverse

from django.contrib.auth import login
from app.modelos.models import usuario,Hoteles

from app.hotel.views import super_usuarios

from app import services

@login_required
def desactivar_activar(request,id):
    user = request.user
    if user.is_superuser:
        try:
            Usuario = usuario.objects.get(pk=id)
            Usuario.cuenta.is_active = not Usuario.cuenta.is_active
            Usuario.cuenta.save()
            return HttpResponseRedirect(reverse(super_usuarios))
        except usuario.DoesNotExist:
            messages.warning(request,"No se puedo realizar la accion compruebe su data")    
            return HttpResponseRedirect("/")
    else:
        messages.error(request,"no puede realizar estas acciones")    
        return HttpResponseRedirect("/")

@login_required
def modificar(request,id):
    user = request.user
    if user.is_superuser:
        try:
            Usuario = usuario.objects.get(pk=id)
            hotel= Hoteles.objects.filter(admin=Usuario)
        except usuario.DoesNotExist:
            return HttpResponseRedirect(reverse("principal"))
    else:
        Usuario =user.usuario
        hotel=Hoteles.objects.filter(admin=user.usuario)


    telefono = Usuario.telefono.split(" ",1)    
    if request.method =='POST':
        form=UsuarioFormEdit(request.POST,instance=Usuario,initial={"username":Usuario.cuenta.username,"tel_cod":telefono[0],"telefono":telefono[1]})#instance=user.usuario
        if form.is_valid():
            datos= form.cleaned_data
            Usuario.nombre = datos.get('nombre')
            Usuario.apellidos=datos.get('apellidos')
            #user.username= datos.get('username')
            Usuario.pais=datos.get('pais')
            Usuario.telefono = datos.get('telefono')
            username=Usuario.cuenta.username
            Usuario.cuenta.username=datos.get('username')
            if username == datos.get('username'):
                Usuario.save()
                Usuario.cuenta.save()
                messages.success(request,"Modificacion exitosa")
                return render(request,"hoteles/guardar.html",context={"titulo":"Modificar usuario","form":[form],'hotel':hotel})
            elif not User.objects.filter(username=datos.get('username')).exists():
                Usuario.save()
                Usuario.cuenta.save()
                messages.success(request,"Modificacion exitosa")
                return render(request,"hoteles/guardar.html",context={"titulo":"Modificar usuario","form":[form],'hotel':hotel})
            else:    
                messages.error(request,"EL nombre de usuario ya existe porfavor pruebe otro")
                return render(request,"hoteles/guardar.html",context={"titulo":"Modificar usuario","form":[form],'hotel':hotel})

        else:
            messages.error(request,"no se pudo modificar")
            return render(request,"hoteles/guardar.html",context={"titulo":"Modificar usuario","form":[form],'hotel':hotel})

    else:  
        telefono = Usuario.telefono.split(" ",1)
        form=UsuarioFormEdit(instance=Usuario,initial={"username":Usuario.cuenta.username,"tel_cod":telefono[0],"telefono":telefono[1]})#instance=user.usuario
        hotel= Hoteles.objects.filter(admin=Usuario).first()
        return render(request,"hoteles/guardar.html",context={"titulo":"Modificar usuario","form":[form],'hotel':hotel})



def load_cities(request):
    country_id = request.GET.get('country')
    region = Region.objects.filter(country_id=country_id).order_by('name')
    return render(request, 'utiles/region.html', {'region': region})


def deslogeo(request):
    logout(request)
    return HttpResponseRedirect('/')


def logeo(request):
    
    if request.method =='POST':
        formLogin=FormularioLogin(request.POST)
        if formLogin.is_valid():
            
            datosform = formLogin.cleaned_data
            
            data =dict(**datosform)
            response = services.logear(data)
            print(response.text)
            if (response.status_code==200):
                if(response.json()["status"] and response.json()["data"]!='' ):
                        usuario = response.json()["data"]
                        if (usuario["is_active"] == 1 ):
                            
                            user = User.objects.get(pk=usuario["id"])
                            if(user.usuario.is_admin):
                                login(request, user)
                                return HttpResponseRedirect("/hotelguardar")
                            else:
                                messages.error(request,"Su cuenta no corresponde a una de administracion de hotel")
                                return HttpResponseRedirect(reverse('home'))    
                        else:
                            messages.error(request,"SU CUENTA ESTA DESACTIVADA")
                            return HttpResponseRedirect(reverse('home'))
                else:
                    messages.error(request,response.json()["informacion"])
                    return HttpResponseRedirect(reverse('home'))
            else:
                messages.error(request,"Error en servicio")
                return HttpResponseRedirect(reverse('home'))
    else:
        return HttpResponseRedirect(reverse('home'))



def vista(request):

    if(Servicios.objects.all().count()<1):
        Servicios.objects.bulk_create([
            Servicios(nombre='WIFI',default=True),
            Servicios(nombre='TV cable',default=True),
            Servicios(nombre='Garaje',default=True),
            Servicios(nombre='Restaurante',default=True),
            Servicios(nombre='Bar',default=True),
            Servicios(nombre='Aire acondicionado',default=True),
            Servicios(nombre='Biblioteca',default=True),
        ])
        
    
    user = request.user
    if  not user.is_anonymous:
        return HttpResponseRedirect("/hotelguardar")
    #ArticleFormSet = formset_factory(Ser, extra=2, max_num=1)
    #formset = ArticleFormSet()
    if request.method =='POST':
        form = CuentaForm(request.POST)
        formUsuario = usuarioForm(request.POST)
        formLogin=FormularioLogin()
        if form.is_valid() and formUsuario.is_valid():
            datosCuenta = form.cleaned_data
            datosUsuario=formUsuario.cleaned_data
            data=dict(**datosCuenta, **datosUsuario,**{"is_superuser":0,"hotel_admin":1})
            respuesta= services.crear_cuenta(data)
            print(respuesta.text)
            if(respuesta.status_code==200):
                if(respuesta.json()["status"]):
                    messages.success(request,respuesta.json()["informacion"])
                else:
                    messages.error(request,respuesta.json()["informacion"])
            else:
                messages.error(request,respuesta.html5())

            return render (request,'index.html',context={'form':[form,formUsuario],'formLogin':formLogin})
        else :
            return render (request,'index.html',context={'form':[form,formUsuario],'formLogin':formLogin,"data":True})

            
    else:
        #form = CuentaForm()        
        form = CuentaForm()
        formUsuario = usuarioForm()
        formLogin = FormularioLogin()

    return render (request,'index.html',context={'form':[form,formUsuario],'formLogin':formLogin})
