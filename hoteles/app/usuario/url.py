
from django.urls import path
from . import views
urlpatterns = [
    path("logout",views.deslogeo),
    path('login', views.logeo),
    path('',views.vista, name="home"),
    path('desactivar_activar/<int:id>',views.desactivar_activar),
    path('ajax/load-cities/', views.load_cities, name='ajax_load_cities'),
    path('modificar/<int:id>',views.modificar),
    #path('modificar_servicio',views.modificar_servicio),
    

]