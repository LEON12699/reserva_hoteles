import requests 
URL_SERVICES ="http://192.168.1.11:8089"




def crear_servicio_hotel(datos):
    # envia el nombre del servico y no el id 
    r= requests.post(URL_SERVICES+'/hotel/guardar_servicio',data=datos);
    return r

def crear_servicio(datos):
    r= requests.post(URL_SERVICES+'/hotel/agregar_servicio',data=datos);
    return r

def getImagenHotel(id_hotel):
    r= requests.get(URL_SERVICES+'/hotel/imagen_hotel/'+id_hotel);
    return r

def post_imagen(datos,id_hotel):
    r = requests.post(URL_SERVICES+'/hotel/archivos/'+id_hotel,files=datos)
    return r


def editar_hotel(datos,id):
    r = requests.put(URL_SERVICES+'/hotel/editar_hotel/'+id,data=datos)
    return r


def guardar_hotel(datos):
    r = requests.post(URL_SERVICES+'/hotel/guardar',data=datos)
    return r 

def logear(datos):
    r = requests.post(URL_SERVICES+'/cuenta/login',data=datos)
    return r

def crear_cuenta(datos):
    r=requests.post(URL_SERVICES+'/cuenta',data=datos)
    return r

def get_cliente():
    
    r = requests.get(URL_SERVICES+'/cliente')
    clientes = r.json()
    lista = {'clientes':clientes,'req':r}
    #print(r.text)
    #print(r.headers)
    return lista

def post_cliente(datos):
    print(requests.post(URL_SERVICES+'/cliente',data=datos))
