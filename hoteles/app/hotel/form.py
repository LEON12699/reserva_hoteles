from app.modelos.models import Hoteles
from django import forms
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string
from app.modelos.models import *


from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import *




class TimeInput(forms.TextInput):
    input_type="time"
    def render(self, name, value, attrs,renderer=None):
        return '%s ' % super(TimeInput, self).render(name, value, attrs,renderer=None)

class calificacionInput(forms.RadioSelect):
    def render(self, name, value, attrs,renderer=None):
            return '%s ' % super(calificacionInput, self).render(name, value, attrs,renderer=None)


class ServiciosForm(forms.Form):

    pagado = forms.BooleanField(required=False,help_text="diga si el servicio tiene un valor extra")
    nombre= forms.CharField(max_length=100,required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
    


class ServiciosFormAdmin(forms.ModelForm):
    class Meta:
        model=Servicios
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        
    
            

class HabitacionForm(forms.ModelForm):
    class Meta:
        model=descripcion_habitacion
        exclude=('hotel',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout=Layout(HTML("<h3>Datos Habitacion</h3><hr>"),
        Row(Div('tipo_habitacion',css_class="col"),Div('numero_camas',css_class="col"))
        ,'descripcion',
        Row(Div("numero_adultos",css_class="col"),Div("numero_niños",css_class="col"))
        )#Div("portada"))


class OfertaForm(forms.ModelForm):
    class Meta:
        model=oferta
        exclude=('creada','habitacion',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout=Layout(HTML("<h3>Datos Oferta</h3><hr>"),
        Row(Div('numero_habitaciones',css_class="col"),Div('finaliza',css_class="col"))
        ,'desayuno',
        Row(Div("precio",css_class="col"),Div("cancelacion",css_class="col")),
        
        )


class HotelForm(forms.ModelForm):

    class Media:
        css ={
            'all': ('css/calificacion.css',)
            }
        js=('js/ajax.js','js/ajax2.js')

    class Meta:
        model= Hoteles;
        exclude=("admin","imagen",)

    def __init__(self, *args, **kwargs):

        choices_trabajos =list(Servicios.objects.filter(default=1).values_list('id', 'nombre'))
        #print(choices_trabajos)
        super().__init__(*args,**kwargs)
        self.fields['salida'].widget = TimeInput()
        self.fields['entrada'].widget = TimeInput()
        self.fields['ciudad'].choices=()
        self.fields['provincia'].choices=()
        self.fields['servicio'] = forms.MultipleChoiceField(choices=choices_trabajos,required=False)
        self.fields['servicio'].widget=forms.CheckboxSelectMultiple()
        self.fields['servicio'].help_text = 'Elija los servicios que ofrece su hotel, puede agregar mas o eliminarlos luego.'
        
        
        
        self.fields['categoria'] = forms.TypedChoiceField(
            label = " Categoria",
            choices = ((1, "*"), (2, "*"),(3, "*"),(4, "*"),(5, "*")),
            coerce = lambda x: (int(x)),
            widget = forms.RadioSelect(),
            required = True,
            
        )   
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Hidden('url',"{% url 'ajax_load_cities' %}",id="id_url"),
           
            "nombre","tipo"
            ,HTML( """<div id="div_id_categoria" class="form-group"> <label for="id_categoria_0" class=" requiredField">
                 Categoria<span class="asteriskField">*</span> </label> <div class=""> <div class="custom-control custom-radio"> <p class="clasificacion">
    <input id="radio1" type="radio" name="categoria" value="5"><!--
    -->
    <label class ="lbl" for="radio1">★</label><!--
    --><input id="radio2" type="radio" name="categoria" value="4"><!--
    --><label class ="lbl" for="radio2">★</label><!--
    --><input id="radio3" type="radio" name="categoria" value="3"><!--
    --><label class ="lbl" for="radio3">★</label><!--
    --><input id="radio4" type="radio" name="categoria" value="2"><!--
    --><label class ="lbl" for="radio4">★</label><!--
    --><input id="radio5" type="radio" name="categoria" value="1" checked><!--
    --><label class ="lbl" for="radio5">★</label>
  </p></div>"""),Row(Div("entrada",css_class="col"),Div("salida",css_class="col")),
  "descripcion",
  "pais","provincia","ciudad",
  Row(Div("direccion",css_class="col"),Div("telefono",css_class="col")),"servicio",HTML("<hr>")

        )


class HotelFormEdit(forms.ModelForm):
    
    class Media:
        css ={
            'all': ('css/calificacion.css',)
            }
        js=('js/ajax.js',)

    class Meta:
        model= Hoteles;
        exclude=("categoria","admin","servicio","imagen",) # talvez añadir aqui cambio de imagen 

    def __init__(self, *args, **kwargs):

        choices_trabajos =list(Servicios.objects.filter(default=1).values_list('id', 'nombre'))
       # print(choices_trabajos)
        super().__init__(*args,**kwargs)
        self.fields['salida'].widget = TimeInput()
        self.fields['entrada'].widget = TimeInput()
        self.fields['ciudad'].choices=()
        #self.fields['provincia'].choices=()

        self.fields['categoria'] = forms.TypedChoiceField(
            label = " Categoria",
            choices = ((1, "*"), (2, "*"),(3, "*"),(4, "*"),(5, "*")),
            coerce = lambda x: (int(x)),
            widget = forms.RadioSelect(),
            required = True,
            
        )   
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout(
            Hidden('url',"{% url 'ajax_load_cities' %}",id="id_url"),
            
            "nombre","tipo"
            ,HTML("""<div id="div_id_categoria" class="form-group"> <label for="id_categoria_0" class=" requiredField">
                 Categoria<span class="asteriskField">*</span> </label> <div class=""> <div class="custom-control custom-radio"> <p class="clasificacion">
    {% if cat == 5 %}
        <input id="radio1" type="radio" name="categoria" checked value="5">
    {% endif %}
    <input id="radio1" type="radio" name="categoria" value="5"><!--
    -->
    <label class ="lbl" for="radio1">★</label><!--
    -->
    {% if cat == 4 %}
    <input id="radio2" type="radio" name="categoria" value="4" checked>
    {% endif %}
    <!--
    -->
    <input id="radio2" type="radio" name="categoria" value="4">
    
    <label class ="lbl" for="radio2">★</label><!--
    -->
    {% if cat == 3 %}
    <input id="radio3"  checked type="radio" name="categoria"  value="3"><!--
    {% endif %}
    <input id="radio3"  type="radio" name="categoria"  value="3"><!--
    --><label class ="lbl" for="radio3">★</label><!--
    -->
    {% if cat == 2 %}
    <input id="radio4" checked type="radio" name="categoria"  value="2">
    {% endif %}
    <input id="radio4" type="radio" name="categoria"  value="2">
    <!--
    --><label class ="lbl" for="radio4">★</label><!--
    -->
    {% if cat == 1 %}
    <input id="radio5" type="radio" name="categoria" value="1" checked>
    {% endif %}
    <input id="radio5" type="radio"  name="categoria" value="1" ><!--
    --><label class ="lbl" for="radio5">★</label>
  </p></div>"""),Row(Div("entrada",css_class="col"),Div("salida",css_class="col")),
  "descripcion",
  "pais","provincia","ciudad",
  Row(Div("direccion",css_class="col"),Div("telefono",css_class="col")),
)