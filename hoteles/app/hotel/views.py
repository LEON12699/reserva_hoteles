from django.shortcuts import render
from app.modelos.models import Hoteles,Servicios_hotel,Servicios,descripcion_habitacion,oferta,usuario,reserva
from app.hotel.form import HotelForm,HotelFormEdit,ServiciosForm,ServiciosFormAdmin,HabitacionForm,OfertaForm
from django.contrib import messages
from app import services
from django.contrib.auth.decorators import login_required

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from django.http import HttpResponseRedirect
from django.urls import reverse
# Create your views here.


@login_required
def super_eliminar_servicios(request,id):
    if request.user.is_superuser:
        try:
            Nservicio = Servicios.objects.get(pk=id)
        except Servicios.DoesNotExist:
            messages.error(request,"No se encontro")
            return HttpResponseRedirect(reverse(super_servicios))

        Nservicio.delete()
        messages.warning(request,"Se ha eliminado correctamene el servicio")
        return HttpResponseRedirect(reverse(super_servicios))
    else:
        messages.error(request,"sin permisos")
        return HttpResponseRedirect(reverse(principal))


@login_required
def super_modificar_servicios(request,id):
    if request.user.is_superuser:
        try:
            Nservicio = Servicios.objects.get(pk=id)
        except Servicios.DoesNotExist:
            messages.error(request,"No se encontro")
            return HttpResponseRedirect(reverse(super_servicios))


        if request.method=='POST':
            form =ServiciosFormAdmin(request.POST,instance=Nservicio)
            if form.is_valid():
                data = form.cleaned_data
                Nservicio.nombre = data.get("nombre")
                Nservicio.default = data.get("default")
                messages.success(request,"modificado con exito")
                Nservicio.save()
                return HttpResponseRedirect(reverse(super_servicios))
            else:
                return render(request,"hoteles/guardar.html",context={"titulo":"Modificar servicios general","form":[form],"boton":"Editar"})
        else:
            form = ServiciosFormAdmin(instance=Nservicio)
            return render(request,"hoteles/guardar.html",context={"titulo":"Modificar servicios general","form":[form],"boton":"Editar"})
    else:
        messages.error(request,"sin permisos")
        return HttpResponseRedirect(reverse(principal))


@login_required
def super_agregar_servicios(request):
    if request.user.is_superuser:
        if request.method=='POST':
            form =ServiciosFormAdmin(request.POST)
            if form.is_valid():
                data = form.cleaned_data
                Nservicio =Servicios()
                Nservicio.nombre = data.get("nombre")
                Nservicio.default = data.get("default")
                messages.success(request,"agregado con exito")
                Nservicio.save()
                return HttpResponseRedirect(reverse(super_servicios))
            else:
                return render(request,"hoteles/guardar.html",context={"titulo":"Agregar servicios general","form":[form]})
        else:
            form = ServiciosFormAdmin()
            return render(request,"hoteles/guardar.html",context={"titulo":"Agregar servicios general","form":[form]})
    else:
        messages.error(request,"sin permisos")
        return HttpResponseRedirect(reverse(principal))


@login_required
def super_servicios(request):
    user = request.user
    if user.is_superuser:
        lista= Servicios.objects.all()
        return render(request,"utiles/super_tabla_servicios.html",context={"tabla":lista,"titulo":"Servicios"})
    else:
        return HttpResponseRedirect(reverse(principal))


@login_required
def super_usuarios(request):
    user = request.user
    if user.is_superuser:
        lista= usuario.objects.all()
        return render(request,"usuarios/super_tabla_usuarios.html",context={"tabla":lista,"titulo":"Usuarios"})
    else:
        return HttpResponseRedirect(reverse(principal))


@login_required
def super_hoteles(request):
    user = request.user
    if user.is_superuser:
        hoteles= Hoteles.objects.all()
        return render(request,"hoteles/super_tabla_hotel.html",context={"tabla":hoteles,"titulo":"Hoteles"})
    else:
        return HttpResponseRedirect(reverse(principal))


@login_required
def eliminar_oferta(request,id):
    user = request.user
    if user.is_superuser:
        try:
            offer = oferta.objects.get(pk=id)
            hotel= offer.habitacion.hotel
        except oferta.DoesNotExist:
            messages.warning(request,"No tiene acceso o no existe")
            return HttpResponseRedirect(reverse(principal))            
    else:
        if Hoteles.objects.filter(admin_id=user.usuario.id).exists():
            hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
            offer= oferta.objects.filter(pk=id).filter(habitacion__hotel=hotel).first()
        else:
            return HttpResponseRedirect(reverse(principal))            
    
    if oferta.objects.filter(habitacion=offer.habitacion).count()==1:
        offer.habitacion.delete()

    offer.delete()
    messages.success(request,"se a eliminado correctamente")

    return HttpResponseRedirect("/hotel/ofertas?nombre="+hotel.nombre)


@login_required
def tabla_oferta(request):
    user = request.user
    if not user.is_superuser:
        if Hoteles.objects.filter(admin_id=user.usuario.id).exists():
            hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
        else:
            return HttpResponseRedirect(reverse(principal))
    elif request.get_full_path().__contains__("?nombre=") and request.GET["nombre"] !="":
        h_nombre = request.GET['nombre']
        if Hoteles.objects.filter(nombre=h_nombre).exists():
            hotel=Hoteles.objects.filter(nombre=h_nombre).first()
        else:
            return HttpResponseRedirect(reverse(principal))
    elif Hoteles.objects.filter(admin_id=user.usuario.id).exists():
        hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
    
    else:
        return HttpResponseRedirect(reverse(principal))

    try:
        habitacion=descripcion_habitacion.objects.filter(hotel=hotel)
        offer = oferta.objects.filter(habitacion__in=habitacion)
    except descripcion_habitacion.DoesNotExist:
        messages.warning(request,"No tiene acceso o no existe")
        return HttpResponseRedirect(reverse(principal))

    
    
    return render(request,"hoteles/tabla_ofertas.html",context={"tabla":offer,"titulo":"Ofertas","hotel":hotel})
        
    
@login_required
def TablaReservas(request):
    user = request.user
    if not user.is_superuser:
        if Hoteles.objects.filter(admin_id=user.usuario.id).exists():
            hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
        else:
            return HttpResponseRedirect(reverse(principal))
    elif request.get_full_path().__contains__("?nombre=") and request.GET["nombre"] !="":
        h_nombre = request.GET['nombre']
        if Hoteles.objects.filter(nombre=h_nombre).exists():
            hotel=Hoteles.objects.filter(nombre=h_nombre).first()
        else:
            return HttpResponseRedirect(reverse(principal))
    elif Hoteles.objects.filter(admin_id=user.usuario.id).exists():
        hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
    
    else:
        return HttpResponseRedirect(reverse(principal))

    try:
        data=descripcion_habitacion.objects.filter(hotel=hotel)
        ofertas= oferta.objects.filter(habitacion__in=data)
        tabla=reserva.objects.filter(oferta__in=ofertas)

    except descripcion_habitacion.DoesNotExist:
        messages.warning(request,"No tiene acceso o no existe")
        return HttpResponseRedirect(reverse(principal))

    
    return render(request,"hoteles/tabla_reservas.html",context={"tabla":tabla,"titulo":"Reservas de mi hotel","hotel":hotel})
    


@login_required
def modficar_oferta(request,id):
    user=request.user
    if not user.is_superuser:
        if Hoteles.objects.filter(admin_id=user.usuario.id).exists():
            hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
        else:
            return HttpResponseRedirect(reverse(principal))
    elif request.get_full_path().__contains__("?nombre=") and request.GET["nombre"] !="":
        h_nombre = request.GET['nombre']
        if Hoteles.objects.filter(nombre=h_nombre).exists():
            hotel=Hoteles.objects.filter(nombre=h_nombre).first()
        else:
            return HttpResponseRedirect(reverse(principal))

    else:
        return HttpResponseRedirect(reverse(principal))
    

    try:
        habitacion=descripcion_habitacion.objects.filter(pk=id).filter(hotel=hotel).first()
        offer = oferta.objects.filter(habitacion=habitacion).first()
    except descripcion_habitacion.DoesNotExist:
        messages.warning(request,"No tiene acceso o no existe")
        return HttpResponseRedirect(reverse(principal))



    if request.method =='POST':
        formHabitacion=HabitacionForm(request.POST,instance=habitacion)
        formOferta=OfertaForm(request.POST,instance=offer)
        if formHabitacion.is_valid() and formOferta.is_valid():
            data_habitacion= formHabitacion.cleaned_data
            data_oferta = formOferta.cleaned_data  
            if oferta.objects.filter(habitacion=habitacion).count()>1:
                data_habitacion["hotel"]=hotel
                try:
                    habitacion = descripcion_habitacion.objects.get(
                    tipo_habitacion=data_habitacion.get('tipo_habitacion'),
                    hotel=data_habitacion.get('hotel'),
                    numero_adultos=data_habitacion.get('numero_adultos'),
                    numero_niños=data_habitacion.get('numero_niños'),
                    numero_camas=data_habitacion.get('numero_camas'))
                    offer.habitacion = habitacion
                except descripcion_habitacion.DoesNotExist:
                    habitacion = descripcion_habitacion(tipo_habitacion=data_habitacion.get('tipo_habitacion'),
                    hotel=data_habitacion.get('hotel'),
                    numero_adultos=data_habitacion.get('numero_adultos'),
                    numero_niños=data_habitacion.get('numero_niños'),
                    numero_camas=data_habitacion.get('numero_camas'))
                    habitacion.save()
                    offer.habitacion=habitacion
            else:
                data_habitacion["hotel"]=hotel
                try:
                    habitacion2 = descripcion_habitacion.objects.get(
                    tipo_habitacion=data_habitacion.get('tipo_habitacion'),
                    hotel=data_habitacion.get('hotel'),
                    numero_adultos=data_habitacion.get('numero_adultos'),
                    numero_niños=data_habitacion.get('numero_niños'),
                    numero_camas=data_habitacion.get('numero_camas'))

                    #habitacion.delete()
                    offer.habitacion = habitacion2
                except descripcion_habitacion.DoesNotExist:
                    habitacion.tipo_habitacion=data_habitacion.get('tipo_habitacion')
                    habitacion.numero_adultos=data_habitacion.get('numero_adultos')
                    habitacion.numero_niños=data_habitacion.get('numero_niños')
                    habitacion.numero_camas=data_habitacion.get('numero_camas')
                    habitacion.save()
                

            offer.numero_habitaciones=data_oferta.get('numero_habitaciones')
            offer.finaliza=data_oferta.get('finaliza')
            offer.precio=data_oferta.get('precio')
            offer.cancelacion=data_oferta.get('cancelacion')
            offer.desayuno=data_oferta.get('desayuno')
            offer.save()
            messages.success(request,"Se modifico con exito la oferta")
            # cambiar por lista de ofertas una tabla
            return render(request,"hoteles/guardar.html",context={"form":[formHabitacion,formOferta],"titulo":"Modificar Oferta","hotel":hotel,"boton":"Editar"})
        else:
            messages.error(request,"No se pudo agregar la oferta")
            return render(request,"hoteles/guardar.html",context={"form":[formHabitacion,formOferta],"titulo":"Modificar Oferta","hotel":hotel,"boton":"Editar"})

    else:
        formHabitacion=HabitacionForm(instance=habitacion)
        formOferta=OfertaForm(instance=offer)
        return render(request,"hoteles/guardar.html",context={"form":[formHabitacion,formOferta],"titulo":"Modficar oferta","hotel":hotel,"boton":"Editar"})


@login_required
def agregar_oferta(request):
    user=request.user
    if not user.is_superuser:
        if Hoteles.objects.filter(admin_id=user.usuario.id).exists():
            hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
        else:
            return HttpResponseRedirect(reverse(principal))
    elif request.get_full_path().__contains__("?nombre=") and request.GET["nombre"] !="":
        h_nombre = request.GET['nombre']
        if Hoteles.objects.filter(nombre=h_nombre).exists():
            hotel=Hoteles.objects.filter(nombre=h_nombre).first()
        else:
            pass
    else:
        return HttpResponseRedirect(reverse(principal))
    
    
    if request.method =='POST':
        formHabitacion=HabitacionForm(request.POST)
        formOferta=OfertaForm(request.POST)
        if formHabitacion.is_valid() and formOferta.is_valid():
            data_habitacion= formHabitacion.cleaned_data
            data_oferta = formOferta.cleaned_data  
            data_habitacion["hotel"]=hotel
            print(data_habitacion)
            try:
                habitacion = descripcion_habitacion.objects.get(
                    tipo_habitacion=data_habitacion.get('tipo_habitacion'),
                    hotel=data_habitacion.get('hotel'),
                    numero_adultos=data_habitacion.get('numero_adultos'),
                    numero_niños=data_habitacion.get('numero_niños'),
                    #descripcion=data_habitacion.get('descripcion'),
                    numero_camas=data_habitacion.get('numero_camas'))
            except descripcion_habitacion.DoesNotExist:
                habitacion = descripcion_habitacion(tipo_habitacion=data_habitacion.get('tipo_habitacion'),
                    hotel=data_habitacion.get('hotel'),
                    numero_adultos=data_habitacion.get('numero_adultos'),
                    numero_niños=data_habitacion.get('numero_niños'),
                    #descripcion=data_habitacion.get('descripcion'),
                    numero_camas=data_habitacion.get('numero_camas'))
                habitacion.save()
            
            #print(habitacion)
            offer = oferta()
            offer.habitacion=habitacion
            offer.numero_habitaciones=data_oferta.get('numero_habitaciones')
            offer.finaliza=data_oferta.get('finaliza')
            offer.precio=data_oferta.get('precio')
            offer.cancelacion=data_oferta.get('cancelacion')
            offer.desayuno=data_oferta.get('desayuno')
            offer.save()
            messages.success(request,"Se agrego con exito la oferta")
            # cambiar por lista de ofertas una tabla
            return HttpResponseRedirect('/hotel/ofertas?nombre='+hotel.nombre)
        else:
            messages.error(request,"No se pudo agregar la oferta")
            return HttpResponseRedirect('/hotel/ofertas?nombre='+hotel.nombre)
            

    else:
        formHabitacion=HabitacionForm()
        formOferta=OfertaForm()
        return render(request,"hoteles/guardar.html",context={"form":[formHabitacion,formOferta],"titulo":"Agregar oferta","hotel":hotel})




@login_required
def modificar_servicio(request,id):
    try:
        servicio=Servicios_hotel.objects.get(pk=id)
        servicio.pagado = not servicio.pagado
        servicio.save()
        messages.warning(request,"cambio exitoso")

    except Servicios_hotel.DoesNotExist:
        messages.error(request , 'error en busqueda de servicio')
        return HttpResponseRedirect(reverse(principal))
        
        
    return HttpResponseRedirect("/hotel/servicios_hotel?nombre="+servicio.hotel.nombre)

                


@login_required
def agregar_servicio_hotel(request):
    user = request.user
    if not user.is_superuser:
        if Hoteles.objects.filter(admin_id=user.usuario.id).exists():
            hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
        else:
            return HttpResponseRedirect(reverse(principal))
    elif request.get_full_path().__contains__("?nombre=") and request.GET["nombre"] !="":
        h_nombre = request.GET['nombre']
        if Hoteles.objects.filter(nombre=h_nombre).exists():
            hotel=Hoteles.objects.filter(nombre=h_nombre).first()
        else:
            pass
    else:
        return HttpResponseRedirect(reverse(principal))

    if request.method =='POST': 
        
        form= ServiciosForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            name=data.get('nombre').upper()
            if Servicios.objects.filter(nombre=name).exists():
                servicia = Servicios.objects.filter(nombre=name)
                if not Servicios_hotel.objects.filter(hotel=hotel,servicios=servicia[0]).exists():
                    s_hotel= Servicios_hotel()  
                    s_hotel.servicios=servicia[0]
                    s_hotel.hotel=hotel
                    s_hotel.pagado=data.get('pagado')
                    s_hotel.save()
                    messages.success(request,"servicio guardado")
                else:
                    messages.warning(request,"servicio ya existe")
                
                return HttpResponseRedirect("/hotel/servicios_hotel?nombre="+hotel.nombre)
            else:
                servicio = Servicios()
                servicio.nombre =name;
                servicio.default=False
                servicio.save()
                
                s_hotel= Servicios_hotel()  
                s_hotel.servicios=servicio
                s_hotel.hotel=hotel
                s_hotel.pagado=data.get('pagado')
                s_hotel.save()
                messages.success(request,"servicio guardado")        
                #tabla= Servicios_hotel.objects.filter(hotel_id=hotel.id)
                return HttpResponseRedirect("/hotel/servicios_hotel?nombre="+hotel.nombre)
        else:
            return render(request,'hoteles/guardar.html',context={'titulo':"Agregar servicio","form":[form],'hotel':hotel})
    else:
        form= ServiciosForm()
    
    return render(request,'hoteles/guardar.html',context={'titulo':"Agregar servicio","form":[form],'hotel':hotel})



@login_required
def delete(request,id):
    uno=Servicios_hotel.objects.get(pk=id)
    hotel =Hoteles.objects.get(pk=uno.hotel_id)
    uno.delete()
    messages.success(request,"Servicio eliminado")   
    return HttpResponseRedirect("/hotel/servicios_hotel?nombre="+hotel.nombre)

@login_required
def servicios_hotel(request):
    user = request.user
    if not user.is_superuser:
        if Hoteles.objects.filter(admin_id=user.usuario.id).exists():
            hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
        else:
            return HttpResponseRedirect(reverse(principal))
    elif request.get_full_path().__contains__("?nombre=") and request.GET["nombre"] !="":
        h_nombre = request.GET['nombre']
        if Hoteles.objects.filter(nombre=h_nombre).exists():
            hotel=Hoteles.objects.filter(nombre=h_nombre).first()
        else:
            pass
    else:
        return HttpResponseRedirect(reverse(principal))
    
    tabla= Servicios_hotel.objects.filter(hotel_id=hotel.id)

    return render(request,'hoteles/tablas.html',context={'titulo':"Servicios","tabla":tabla,"hotel":hotel})



@csrf_exempt
def archivo(request):

    user = request.user
    if not user.is_superuser:
        if Hoteles.objects.filter(admin_id=user.usuario.id).exists():
            hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
        else:
            return HttpResponseRedirect(reverse(principal))

    elif request.get_full_path().__contains__("?nombre=") and request.GET["nombre"] !="":
        h_nombre = request.GET['nombre']
        if Hoteles.objects.filter(nombre=h_nombre).exists():
            hotel=Hoteles.objects.filter(nombre=h_nombre).first()
        else:
            pass
    else:
        return HttpResponseRedirect(reverse("principal"))

    if request.method =='POST':
        services.post_imagen(request.FILES,str(hotel.id))
        return HttpResponse(200,content_type='text/plain')
    else:
        r = services.getImagenHotel(str(hotel.id))
        imagenes=r.json()["data"];
        return render(request,'prueba.html',context={'hotel':hotel,'titulo':"guardar imagenes","URL":services.URL_SERVICES,"imagenes":imagenes})


@login_required(login_url="/")
def principal(request):
    user = request.user
    url =services.URL_SERVICES
    #imagen=["una","dos","tres"]
    if not user.is_superuser:
        if Hoteles.objects.filter(admin_id=user.usuario.id).exists():
            hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
            r = services.getImagenHotel(str(hotel.id));
            imagenes=r.json()["data"]
            servicios_h = Servicios_hotel.objects.filter(hotel_id=hotel.id)
            return render(request,"usuarios/principal.html",context={"hotel":hotel,"URL":url,"imagenes":imagenes,"servicios":servicios_h})
    elif request.get_full_path().__contains__("?nombre=") and request.GET["nombre"] !="":
        h_nombre = request.GET['nombre']
        if Hoteles.objects.filter(nombre=h_nombre).exists():
            hotel=Hoteles.objects.filter(nombre=h_nombre).first()
            r = services.getImagenHotel(str(hotel.id));
            imagenes=r.json()["data"]
            servicios_h = Servicios_hotel.objects.filter(hotel_id=hotel.id)
            return render(request,"usuarios/principal.html",context={"hotel":hotel,"URL":url,"imagenes":imagenes,"servicios":servicios_h})
        else:
            pass
    elif Hoteles.objects.filter(admin_id=user.usuario.id).exists():
            hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
            r = services.getImagenHotel(str(hotel.id));
            imagenes=r.json()["data"]
            servicios_h = Servicios_hotel.objects.filter(hotel_id=hotel.id)
            return render(request,"usuarios/principal.html",context={"hotel":hotel,"URL":url,"imagenes":imagenes,"servicios":servicios_h})

    else:
        pass
    return render(request,"usuarios/principal.html")#context={"imagenes":imagen})



def imagen_principal(request):
    user = request.user
    if (Hoteles.objects.filter(admin_id=user.usuario.id).exists() and (user.usuario.is_admin or user.is_superuser)) :
        hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
        hotel.imagen = request.GET["imagen"]
        hotel.save()
        messages.success(request,"La imagen ahora es la principal en sus vistas")
        return HttpResponseRedirect(reverse(archivo))
    else:
        messages.error(request,"No se pudo completar el proceso, verifique sus permisos o comuniquese con sistemas")
        return HttpResponseRedirect(reverse(archivo))





@login_required
def guardar_hotel(request):
    user = request.user
    if Hoteles.objects.filter(admin_id=user.usuario.id).exists() :
        hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
        return HttpResponseRedirect("/hotel?nombre="+hotel.nombre)

    if request.method =='POST':
        form = HotelForm(request.POST)
        if form.is_valid():
            datos = form.cleaned_data   
            datos["pais_id"] = datos["pais"].id
            datos["provincia_id"] = datos["provincia"].id
            
            datos["admin_id"]= request.user.usuario.id  
            datos["servicio"]=str(datos["servicio"]).replace("[","").replace("]","").replace("'","").replace(" ","")
            respuesta= services.guardar_hotel(datos)
            print(respuesta.text)
            contexto={"form":[form],"titulo":"Crear Hotel"}
            if(respuesta.json()["status"]):
                messages.success(request,respuesta.json()["informacion"])
                return  HttpResponseRedirect('/')
            else:
                messages.error(request,respuesta.json()["informacion"])
        else:
            form = HotelForm(request.POST)
            contexto={"form":[form],"titulo":"Crear Hotel"}
    else:
        form = HotelForm()
        contexto={"form":[form],"titulo":"Crear Hotel"}
    
    return render(request,"hoteles/guardar.html",context=contexto)

# eidicin ed editar de hotal

@login_required
def editar_hotel(request):
    h_nombre = request.GET['nombre']
    user = request.user
    if not user.is_superuser:
        if Hoteles.objects.filter(admin_id=user.usuario.id).exists():
            hotel = Hoteles.objects.filter(admin_id=user.usuario.id).first()
    elif Hoteles.objects.filter(nombre=h_nombre).exists():
        hotel = Hoteles.objects.filter(nombre=h_nombre).first()
    else:
        return HttpResponseRedirect(reverse("principal"))

    if request.method =='POST':
        form = HotelFormEdit(request.POST,instance=hotel)
        
        if form.is_valid():
            datos = form.cleaned_data
            enviar_datos={}
            dat= form.changed_data
            for field in dat:
               # print(field)
                if(field =="provincia" or field=="pais"):
                    enviar_datos[field+"_id"]=datos[field].id
                else:
                    enviar_datos[field]= datos[field]

            #print (enviar_datos)
            respuesta = services.editar_hotel(enviar_datos,str(hotel.id))
            print(respuesta.text)
            if(respuesta.status_code==200):
                if(respuesta.json()["status"]):            
                    messages.success(request,respuesta.json()["informacion"])
                    return  HttpResponseRedirect("/hotel?nombre="+hotel.nombre)
                else:
                    messages.error(request,respuesta.json()["informacion"])
                    return  HttpResponseRedirect("/hotel?nombre="+hotel.nombre)
            else:
                messages.error(request,"Error en el servidor ")
                return  HttpResponseRedirect("/hotel?nombre="+hotel.nombre)
        else:
            messages.warning(request,"No se han detectado cambios ")
            return render(request,"hoteles/guardar.html",context={"boton":"editar","titulo":"Editar hotel","form":[form],"cat":hotel.categoria})
    else:
        form =HotelFormEdit(instance=hotel)
        return render(request,"hoteles/guardar.html",context={"form":[form],"cat":hotel.categoria,"titulo":"Editar hotel","boton":"editar"})

        
        
