
from django.urls import path
from . import views
urlpatterns = [
    path('/super_eliminar_servicios/<int:id>',views.super_eliminar_servicios),
    
    path('/super_modificar_servicios/<int:id>',views.super_modificar_servicios),
    path('/super_agregar_servicios',views.super_agregar_servicios),
    path('/super_servicios',views.super_servicios),
    path('/super_usuarios',views.super_usuarios),
    path('/super_hoteles',views.super_hoteles),
    path('',views.principal, name="principal"),
    path('guardar',views.guardar_hotel, name="hotel_guarda"),
    path('editar_hotel',views.editar_hotel, name="hotel_editar"),
    path('image',views.archivo, name="imagen"),
    path('/servicios_hotel',views.servicios_hotel),
    path('/eliminar/<int:id>',views.delete),
    path('/agregar_servicio',views.agregar_servicio_hotel),
    path('/modificar/<int:id>',views.modificar_servicio),
    path('/agregar_oferta',views.agregar_oferta),
    path('/modficar_oferta/<int:id>',views.modficar_oferta),
    path('/ofertas',views.tabla_oferta),
    path('/oferta_eliminar/<int:id>',views.eliminar_oferta),
    path('/activar_imagen',views.imagen_principal),
    path('/reservas',views.TablaReservas),
]