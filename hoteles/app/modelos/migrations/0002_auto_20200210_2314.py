# Generated by Django 2.2.6 on 2020-02-10 23:14

import datetime
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modelos', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='descripcion_habitacion',
            name='camas',
        ),
        migrations.AddField(
            model_name='descripcion_habitacion',
            name='numero_camas',
            field=models.IntegerField(default=1, validators=[django.core.validators.MinValueValidator(1)], verbose_name='numero de camas en la habitacion'),
        ),
        migrations.AlterField(
            model_name='oferta',
            name='finaliza',
            field=models.DateField(validators=[django.core.validators.MinValueValidator(datetime.date(2020, 2, 10), 'el dia debe ser mayor al dia actual')]),
        ),
        migrations.AlterField(
            model_name='reserva',
            name='fecha_inicio',
            field=models.DateField(validators=[django.core.validators.MinValueValidator(datetime.date(2020, 2, 10), 'el dia debe ser mayor al dia actual')]),
        ),
    ]
