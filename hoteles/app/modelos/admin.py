from django.contrib import admin

# Register your models here.
from app.modelos.models  import *
from django.forms import ClearableFileInput
from django import forms


'''
class habitacionForm(forms.ModelForm):
	check = forms.BooleanField(label="check",required=False)
	model= habitaciones

class InlineHabitacion(admin.TabularInline):
    	form= habitacionForm
	model= habitaciones
'''

'''
clase inline para imagen habitacion 
'''

'''
clase de administracion de los usuarios
'''
class AdminUsuario(admin.ModelAdmin):
	list_display = [ "cuenta","apellidos", "nombre", "imagen","contactos","is_admin","pais"]
	list_editable = [ "nombre","contactos"]
	#list_display_links = ("apellidos","nombre")
	list_filter = ["pais", "apellidos"]
	search_fields = ["apellidos","pais__name"]
	#radio_fields = {"cuenta": admin.VERTICAL} # este solo es prueba borrar al terminar 
	def get_search_results(self, request, queryset, search_term):
		queryset, use_distinct = super().get_search_results(request, queryset, search_term)
		try:
			search_term_as_int = int(search_term)
			print(request)
			print(queryset)
		except ValueError:
			pass
		else:
			queryset |= self.model.objects.filter(pais=search_term_as_int)
		return queryset, use_distinct
	
	class Meta:
		model = usuario

admin.site.register(usuario,AdminUsuario)



class AdminServicios(admin.ModelAdmin):
	list_display = [ "nombre"]
	#list_display_links = ("apellidos","nombre")
	list_filter = ["nombre"]
	search_fields = ["nombre"]
	class Meta:
		model=Servicios

admin.site.register(Servicios,AdminServicios)

'''
clase de administracion de los servicios_hotel
'''

class AdminServicios_hotel(admin.ModelAdmin):
	list_display = [ "hotel","servicios","pagado"]
	#list_display_links = ("apellidos","nombre")
	list_filter = ["hotel","servicios","pagado"]
	search_fields = ["hotel"]
#	radio_fields = {"servicios": admin.VERTICAL} # este solo es prueba borrar al terminar 
	
	class Meta:
		model = Servicios_hotel

admin.site.register(Servicios_hotel,AdminServicios_hotel)

'''
clase de administracion de imagen 
'''

#class InlineImagen(admin.TabularInline):
#	model = imagen_hotel

'''
class AdminCama(admin.ModelAdmin):
	list_display = [ "nombre"]		
	class Meta:
		model = camas

admin.site.register(camas,AdminCama)
'''

'''
clase de administracion de descripcion_hoteles


class AdminDescripcion_habitacion(admin.ModelAdmin):
    
	list_display = [ "tipo_habitacion","numero_adultos", "numero_niños", "descripcion","get_camas"]
	#list_display_links = ("apellidos","nombre")
	list_filter = [ "tipo_habitacion","numero_adultos", "numero_niños","camas"]	
	search_fields = ["tipo_habitacion"] 
	inlines = [InlineImagenHabitacion]
	def get_camas(self,obj):
		print(obj.camas.all())
		return "\n".join([p.nombre for p in obj.camas.all()])
	
admin.site.register(descripcion_habitacion,AdminDescripcion_habitacion)
'''
'''
clase de administracion de habitaciones 
'''


    
class Inlineservices(admin.TabularInline):
	numero =Servicios.objects.all().count()
	model = Servicios_hotel
	extra= numero 


'''
clase ded admin de hotel
'''

class AdminHotel(admin.ModelAdmin):
	list_display = [ "nombre","categoria", "tipo", "direccion","provincia","pais","ciudad","telefono","descripcion","admin","get_servicios","entrada","salida"]	
	#list_display_links = ("apellidos","nombre")
	list_filter = ["nombre","categoria","pais","ciudad","tipo"]
	search_fields = ["nombre","categoria","tipo","pais"] 
	#inlines=[InlineImagen,Inlineservices]
	def get_servicios(self,obj):
		print(obj.servicio.all())
		return "\n".join([p.nombre for p in obj.servicio.all()])
admin.site.register(Hoteles,AdminHotel)





class AdminReserva(admin.ModelAdmin):
	list_display = [ "usuario","oferta", "fecha_inicio", "fecha_hasta"]	
	#list_display_links = ("apellidos","nombre")
	list_filter = ["fecha_inicio","oferta","usuario"]
	search_fields = ["usuario","oferta"] 
	class Meta:
		model= reserva
admin.site.register(reserva,AdminReserva)



class AdminOferta(admin.ModelAdmin):
	list_display = [ "habitacion", "numero_habitaciones", "creada","finaliza","precio","cancelacion","desayuno"]	
	#list_display_links = ("apellidos","nombre")
	list_filter = ["finaliza","precio","desayuno"]
	search_fields = ["habitacion","cancelacion","precio"]
	''' 
	inlines=(InlineHabitacion,)
	def save_formset(self, request, form, formset, change):
		for habitacion_form in formset:
			if habitacion_form.cleanded_data.get('check') ==True: 
				form.instance.save()
		super(AdminOferta,self).save_formset(request, form, formset, change)
	'''
	class Meta:
		model= oferta
admin.site.register(oferta,AdminOferta)


'''
class AdminPromocion(admin.ModelAdmin):
	list_display = [ "oferta", "creada", "finaliza","porcentaje","valor"]	
	#list_display_links = ("apellidos","nombre")
	list_filter = ["oferta","creada","porcentaje"]
	search_fields = ["oderta","finaliza","creada"]
admin.site.register(promocion,AdminPromocion)
'''

