from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator,MinLengthValidator
from cities_light.abstract_models import (AbstractCity, AbstractRegion,
    AbstractCountry)
from cities_light.receivers import connect_default_signals
from django.contrib.auth.models import User
import uuid
import datetime
from django.core.exceptions import ValidationError
from threading import Event
from django.utils import timezone


'''
para documentar usar pip install docutils html, xml, latex
'''

class Country(AbstractCountry):
    pass
connect_default_signals(Country)

class Region(AbstractRegion):
    pass
connect_default_signals(Region)


class City(AbstractCity):
    pass
connect_default_signals(City)



# Create your models here.

class Servicios(models.Model):
    nombre = models.CharField(max_length=100,null=False,unique=True)
    default=models.BooleanField(verbose_name="aparece por defecto",default=False)
    def __str__(self):
        return '%s' % (self.nombre) 
    

class usuario(models.Model):
    #choices = Country.objects.all()
    cuenta =models.OneToOneField(User,on_delete=models.CASCADE) 
    nombre = models.CharField(null=False, max_length= 100)
    apellidos = models.CharField(null=False, max_length= 100)
    imagen = models.ImageField(verbose_name='imagen de usuario',upload_to="usuarios",default="usuarios/no_imagen.png",null=True)
    contactos = models.EmailField(null=True, max_length = 125)
    is_admin = models.BooleanField(null=False)
    external = models.UUIDField(default=uuid.uuid4, editable=False)  # este puede ser util dependiendo
    pais = models.ForeignKey(Country,on_delete=models.DO_NOTHING)
    telefono = models.CharField(validators=[MinLengthValidator(9,"ingrese un telefono valido")],max_length=100,verbose_name="telefono para contactarlo para su reserva",null=False)
    def __str__(self):
        return '%s' % (self.nombre +" "+ self.apellidos) 
    
    def save(self, *args, **kwargs):
        self.telefono = self.pais.phone +" "+self.telefono
        super(usuario,self).save(*args, **kwargs)


class Hoteles(models.Model):
    nombre = models.CharField("Nombre comercial",max_length = 50, null = False,unique=True)
    categoria = models.IntegerField(null = False,validators=[MinValueValidator(1), MaxValueValidator(5)])	
    tipo = models.CharField(max_length = 15, null = False)
    direccion = models.CharField(max_length = 100, null = False)
    provincia = models.ForeignKey(Region , on_delete= models.CASCADE)
    pais = models.ForeignKey( Country ,on_delete=models.CASCADE)
    ciudad = models.CharField("Ciudad",null=True,blank=True,max_length=100)
    telefono = models.CharField(max_length =14, blank= True )
    descripcion = models.TextField( blank= True) 
    servicio = models.ManyToManyField("Servicios",verbose_name='servicios_hotel',through="Servicios_hotel",through_fields=('hotel','servicios'))
    admin = models.ForeignKey(usuario,on_delete=models.CASCADE)
    entrada = models.TimeField(verbose_name='hora de arribo')
    salida = models.TimeField(verbose_name='hora de salida')
    imagen = models.ImageField(null=True)
    
    def __str__(self):
        return '%s' % (self.nombre) 


    
    
    #verificar el metodo si funciona




#class camas(models.Model):
#    nombre = models.CharField(max_length=254,null=False)

    #describir un nombre para ver si das opcion a agregar tipo de camas   desayuno = models.BooleanField(null=False,verbose_name='incluye desayuno')  
#    def __str__(self):
#        return '%s' %(self.nombre)
    




class Servicios_hotel(models.Model):  
    pagado = models.BooleanField('costo adicional',null=False)
    servicios = models.ForeignKey("Servicios",on_delete=models.CASCADE)
    hotel = models.ForeignKey("Hoteles",on_delete=models.CASCADE)


# empleados = models.ManyToManyField('usuario',through='empleado', through_fields=('hotel', 'persona'))
# todavia por arreglar la parte de camas esta de arreglar 

class descripcion_habitacion(models.Model):
    tipo_habitacion = models.CharField(max_length=254,null=False) #poner choices 	
    numero_adultos= models.IntegerField(null = False,validators=[MinValueValidator(1), MaxValueValidator(30)],verbose_name='adultos en la habitacion')
    numero_niños= models.IntegerField(null = True,validators=[MinValueValidator(0), MaxValueValidator(30)],verbose_name='niños en la habitacion')
    #portada=models.ImageField(null=False)
    #descripcion = models.TextField(verbose_name='descripcion de la habitacion',default='habitacion')
    #camas = models.ManyToManyField(camas,related_name='camas_habitacion',verbose_name='camas',null=False)
    numero_camas=models.IntegerField(null=False,verbose_name="numero de camas en la habitacion",default=1,validators=[MinValueValidator(1)])
    hotel = models.ForeignKey("Hoteles",on_delete=models.CASCADE) #,editable=False)
    
    def __str__(self):
        return '%s' % (self.tipo_habitacion) 


class imagen_hotel(models.Model):
    hotel = models.ForeignKey(Hoteles, verbose_name="hotel", on_delete=models.CASCADE)
    imagen = models.ImageField(null=False,upload_to="hoteles")

# revisar estro parece hay mejor forma de hacerlo

#class imagen_habitacion(models.Model):
#    descripcion_habitacion = models.ForeignKey(descripcion_habitacion,on_delete=models.CASCADE)
#    imagen = models.ImageField(null=False,upload_to="hoteles")


# revison de habitaciones por 
#class habitaciones(models.Model):
#    datos = models.ForeignKey(descripcion_habitacion, verbose_name="datos_habitacion", on_delete=models.CASCADE)
    # pensar esto models.ForeignKey(oferta, verbose_name=("ofertar habitacion"), on_delete=models.SET_NULL)    
#    hotel = models.ForeignKey(Hoteles,on_delete=models.CASCADE)#editable=False)
#    oferta = models.ForeignKey("oferta", verbose_name=("oferta de la habitacion"), on_delete=models.CASCADE)


class oferta(models.Model):
    # esto se analiza 
    habitacion = models.ForeignKey("descripcion_habitacion",on_delete=models.CASCADE,related_name="oferta")
    numero_habitaciones =models.PositiveIntegerField(null=False,verbose_name="numero de habitaciones que dispone para ofertar:")
    creada= models.DateField(auto_now=True,null=False)
    finaliza = models.DateField(null=False,validators=[MinValueValidator(datetime.date.today(),"el dia debe ser mayor al dia actual")])
    precio = models.DecimalField(max_digits=10,decimal_places=2,verbose_name="precio por noche $")
    cancelacion = models.PositiveIntegerField(default=0,null=False, verbose_name="dias previos con los que puede cancelar")
    desayuno = models.BooleanField(verbose_name='incluye desayuno')
    def save(self, *args, **kwargs):
        if str(self.finaliza) < str(datetime.date.today()):
            raise ValidationError("El dia no puede ser un dia pasado!")
        super(oferta,self).save(*args, **kwargs)

    def __str__(self):
        return '%s %s' % (self.habitacion.hotel, self.habitacion) 



#class empleados(models.Model):
 #   hotel = models.ForeignKey(Hoteles, on_delete=models.CASCADE)
 #   persona = models.ForeignKey(usuario,on_delete=models.CASCADE)



class reserva(models.Model):
    usuario = models.ForeignKey(usuario,on_delete=models.CASCADE)
    oferta = models.ForeignKey("oferta",on_delete=models.CASCADE)
    valor_paga=models.DecimalField('paga',max_digits=10,validators=[MinValueValidator(1)],decimal_places=3)
    fecha_inicio = models.DateField(validators=[MinValueValidator(datetime.date.today(),"el dia debe ser mayor al dia actual")])
    fecha_hasta = models.DateField()
    requerimientos = models.TextField(verbose_name="requerimientos adicionales (contactese con el hotel para su confirmacion )")
    def save(self, *args, **kwargs):
        if str(self.fecha_inicio) < str(datetime.date.today()):
            raise ValidationError("El dia no puede ser un dia pasado!")
        if str(self.fecha_inicio) > str(self.fecha_hasta):
            raise ValidationError("El dia de llegada no puede ser menor al de salida")
        if str(self.fecha_inicio) == str(self.fecha_hasta):
            raise ValidationError("El dia de llegada no puede ser menor al de salida")                
        
        super(reserva,self).save(*args, **kwargs)

'''
validar el porcentaje , con el valor ,a manera de calculo , e impedir se guarde promocion con valor menor al de precio oferta alerta si pone 100 o gratis
probar con el save de arriba

class promocion(models.Model):
    oferta = models.ForeignKey("oferta",on_delete=models.CASCADE)
    creada= models.DateField(auto_now=True,null=False)
    finaliza = models.DateField(null=False)
    porcentaje = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(100)])
    valor = models.DecimalField(max_digits=10,decimal_places=2) #aqui dar opcion de ingrsaer valor o ingresarr porcentaje
    
'''
    

