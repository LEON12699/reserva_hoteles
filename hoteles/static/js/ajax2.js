$(document).ready(function() {
    console.log($("#id_pais").val());
    if ($("#id_pais").val() != "") {
        $.ajax({ // initialize an AJAX request
            url: $("#id_url").val(), // set the url of the request (= localhost:8000/hr/ajax/load-cities/)
            data: {
                'country': $("#id_pais").val() // add the country id to the GET parameters
            },
            success: function(data) { // `data` is the return of the `load_cities` view function
                $("#id_provincia").html(data); // replace the contents of the city input with the data that came from the server
                // $("#id_ciudad").html("<option>-----------</option>");
            }
        });

    }

});