<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Controllers\HotelController;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router ->group(['prefix'=>'paises'], function($router){
    $router ->get('','PaisController@obtener_paises');

    //borrar este de aquu
});

$router ->group(['prefix'=>'cuenta'], function($router){
    $router ->post('','UserController@crear_cuenta');
    $router ->post('/login','UserController@login');

    //borrar este de aquu
});

$router ->group(['prefix'=>'reservas'], function($router){
    $router -> post('','ReservaController@reservar');
    $router -> get('/usuario/{id_usuario}','ReservaController@reservas_usuario');



});

$router ->group(['prefix'=>'hotel'], function($router){
    $router -> post("/archivos/{id_hotel}",'HotelController@archivos');
    $router ->post('/guardar','HotelController@guardar');
    $router ->put('/editar_hotel/{id}','HotelController@editar');
    $router ->get('/imagen_hotel/{id_hotel}','HotelController@obtener_imagenHotel');
    $router ->get('/servicios/{id_hotel}','HotelController@getServiciosHotel');
    $router ->get('/serviciosMovil/{id_hotel}','HotelController@getServiciosHotelMovil');
    $router ->post('/guardar_servicio','HotelController@agregarServicio_hotel');
    $router ->post('/agregar_servicio','HotelController@agregarServicio');
    $router ->get("/obtener_hotel/{id}","HotelController@obtener_hotel");
    $router ->get("/obtener_oferta","HotelController@obtener_oferta");
    $router ->get("/imagenes/{id_hotel}","HotelController@Imagenes") ;
    $router ->get('/get_data_movil/{id_oferta}','HotelController@getData');

});
