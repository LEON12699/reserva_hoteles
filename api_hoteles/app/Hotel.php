<?php
namespace App;


use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Hotel extends Model
{
protected $table= 'modelos_hoteles';
protected $primaryKey = 'id';
/**
* The attributes that are mass assignable.
*
* @var array
*/
protected $fillable = [
'nombre','categoria','tipo','direccion','telefono','descripcion','entrada','salida','admin_id','ciudad','pais_id','provincia_id','imagen'
];


public function usuario(){
    return $this->belongsTo(Usuario::class,"id","admin_id");
}

public function pais(){
    return $this->belongsTo(Paises::class,"pais_id");
}

public function habitaciones(){
    return $this->hasMany(Habitacion::class);
}

public function servicios_hotel(){
    return $this->hasMany(Servicios_hotel::class);
}

public function imagenes(){
    return $this->hasMany(Imagen_hotel::class);
}

##
#public function ciudad(){
#    return $this-> belongsTo(Ciudad::class):
#}

public function provincia(){
    return $this-> belongsTo(Provincia::class,"provincia_id","id");
}



public $timestamps = false;
/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/
/**protected $hidden = [
'password',
];*/
}
