<?php
namespace App;


use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Servicios extends Model
{
protected $table= 'modelos_servicios';
protected $primaryKey = 'id';
/**
* The attributes that are mass assignable.
*
* @var array
*/
protected $fillable = [
'nombre','default',
];


public function Servicios_hotel(){
    return $this->hasMany(Servicios_hotel::class);
}

##
#public function ciudad(){
#    return $this-> belongsTo(Ciudad::class):
#}

#public function provincia(){
#    return $this-> belongsTo(Provincia::class):
#}



public $timestamps = false;
/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/
/**protected $hidden = [
'password',
];*/
}
