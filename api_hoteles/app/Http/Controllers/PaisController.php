<?php

namespace App\Http\Controllers;

use App\Http\helped\responseBuilder;
use Exception;
use App\Paises;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class PaisController extends BaseController
{
    public static function pais($name){

    $p= Paises::select('id',"phone")->where("name",$name)->first();
    if(!empty($p)){
    return $p;}
    else{
        throw new Exception("no existe el pais ");
    }

    }

    public static function obtener_paises(Request $request){

        $p= Paises::select('name')->get();
        if(!empty($p)){
        return responseBuilder::result(true,"recuperacion exitosa",$p);
    }
        else{
            return responseBuilder::result(false,"no se recupero problema del servidor");
        }

        }
}
