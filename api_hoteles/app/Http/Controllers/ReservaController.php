<?php

namespace App\Http\Controllers;

use App\Http\helped\responseBuilder;
use Exception;
use App\Paises;
use App\Reserva;
use App\Oferta;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class ReservaController extends BaseController
{
    public function reservas_usuario(Request $request,$id_usuario){
        $reserva = Reserva::with(["oferta:id,habitacion_id","oferta.habitacion:id,hotel_id","oferta.habitacion.hotel"=> function($query){
        $query->select("id","nombre");}])->where("usuario_id",$id_usuario)->get();


        $status=true;
        $info="Se a realizado la obtencion con exito";
        return responseBuilder::result($status,$info,$reserva);
    }


    public function reservar(Request $request){
        $reserva = new Reserva();
        $reserva->fecha_inicio = $request->fecha_inicio;
        $reserva->fecha_hasta = $request->fecha_hasta;
        $reserva->oferta_id = $request->oferta_id;
        $reserva->usuario_id = $request->usuario_id;
        $reserva->requerimientos = $request->requerimientos;
        $valor= Oferta::select("precio")->where("id",$request->oferta_id)->first();
        $reserva->valor_paga= $valor->precio;

        if($reserva->save()){
            $status=true;
            $info="Se a realizado su reserva con exito su reserva";
            return responseBuilder::result($status,$info,$reserva);
        }
        else{
        $status=false;
        $info="Se a realizado con exito su reserva";
        return responseBuilder::result($status,$info);
        }
    }
}
