<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use illuminate\Hashing\BcryptHasher;
use App\Http\helped\responseBuilder;

use App\User;
use App\Usuario;
use DateTime;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Exception;
use Illuminate\Database\QueryException;
use Faker\Provider\Uuid;
use App\Http\Controllers\PaisController;
class UserController extends Controller
{


    public function crear_cuenta(Request $request) {
        if(User::where("username",$request->username)->count()>0){
            $status = False;
            $info = 'El username ya se encuentra registrado, porfavor pruebe otro';
            return responseBuilder::result($status, $info);
        }
        if(User::where("email",$request->email)->count()>0){
            $status = False;
            $info = 'El correo ya se encuentra registrado';
            return responseBuilder::result($status, $info);
        }
        try{
            $user = new User();
            $user->username =  $request->username;
            $user->password = $this->make_password($request->password);
            $user->last_name = $request->apellidos;
            $user->first_name = $request->nombre;
            $user->is_superuser = $request->is_superuser;
            $user->is_staff  = 1;
            $user->email = $request->email;
            $user->is_active = True;
            $A = new DateTime('now');
            $user->date_joined = Date($A->format('Y-m-d H:i:s.s'));
            $user->last_login =Date($A->format('Y-m-d H:i:s.s'));
            $user->save();

            $usuario = new Usuario();
            $usuario->nombre = $request->nombre;
            $usuario->apellidos = $request->apellidos;
            $usuario->imagen = "/imagenes/no_imagen.png";
            $usuario->contactos = $request->email;
            $usuario->cuenta_id =$user->id;
            $p=PaisController::pais($request->pais);
                try{
                  $usuario->pais_id =$p->id  ; // revisar esto como se envia par ano poner el pais o poner pais pensar por usuario
                }catch(Exception $ex){
                    $user->delete();
                }
               // print_r($p);
            $usuario->telefono = $p->phone." ".$request->telefono;
            $usuario->is_admin=$request->hotel_admin;
            $usuario->external=Uuid::uuid();

            if($usuario -> save() ){
                $status = true;
                    $info = 'Usuario creado';
                    $data = $usuario;

            }

            return responseBuilder::result($status, $info,$data);

    }catch(QueryException $E){
        $usuario->delete();
        $user->delete();
        $status = false;
        echo $E;
        $info = 'Error no creado';
        return responseBuilder::result($status, $info);
    }





    }


    public function login(Request $request){
        $password = $request-> password;
        $username = $request-> username;
        $A = new DateTime('now');


        $user = User::where('username',$username)->orWhere("email",$username)->with("cuenta:id,cuenta_id")->first();

        if(!empty($user)){
            if($this->django_password_verify($password,$user->password)){
                $status = true;
                $info = 'User is correct';
                $user->where('id', $user->id)
                ->update(['last_login' => $A->format('Y-m-d H:i:s.s')
                ]);
                $data=$user;
            }else{
                $status = false;
                $info = 'Credenciales incorrectas';
                $data="";

            }
        }else{
            $status = false;
            $info = 'Credenciales incorrectas';//password mal esta
            $data="";
        }
        return responseBuilder::result($status, $info,$data);
    }
    //

    public function make_password(string $password, $algorithm='sha256', $iterations=15000) : string{
        $salt = base64_encode(openssl_random_pseudo_bytes(9));

        $hash = hash_pbkdf2($algorithm, $password, $salt, $iterations, 32, true);

        return 'pbkdf2_' . $algorithm . '$' . $iterations . '$' . $salt . '$' . base64_encode($hash);
    }




    public function django_password_verify(string $password, string $djangoHash):bool {
        $pieces = explode('$',$djangoHash);
            if(count($pieces) !== 4){
                throw new Exception('ilegaL has format');
            }
            list($header, $iter,$salt,$hash) = $pieces;
            if(preg_match('#^pbkdf2_([a-z0-9A-Z]+)$#',$header,$m)){
                $algo = $m[1];
            }else{
                throw new Exception('BAD HEADER {%s}',$header);

            }
            if (!in_array($algo,hash_algos()) ){
                throw new Exception(sprintf('ilegal hash algoritm (%s)',$algo));

            }
        // hash_pbdk2 -> genera una derivacion de clave pbkdf2 de una contraseñaproporcionada
        // algo es el nombre del algoritmos hash seleccionad (sha256,'')
        //salt = es un valor para la derivacion , Este valor deberia ser generado aletoriamente
        $calc = hash_pbkdf2(
            $algo,
            $password,
            $salt,
            (int)$iter,
            32,
            true
        );

        return hash_equals($calc,base64_decode($hash));


    }
}
