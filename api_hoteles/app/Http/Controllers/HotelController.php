<?php

namespace App\Http\Controllers;

use App\Habitacion;
use Exception;
use App\Hotel;
use App\Imagen_hotel;
use Illuminate\Http\Request;
use App\Http\helped\responseBuilder;
use App\Oferta;
use App\Paises;
use App\Provincia;
use App\Reserva;
use App\Servicios;
use App\Servicios_hotel;

class HotelController extends Controller
{

    public function agregarServicio_hotel(Request $request){
        $servicio_h = new Servicios_hotel();
        $nombre=$request->nombre;
        $servicio = Servicios::firstOrCreate(array('nombre' => $nombre));
        print($servicio);
        $servicio_h->pagado =$request->pagado;
        $servicio_h->hotel_id =$request->hotel_id;
        $servicio_h->servicio_id=$servicio->id;

        $status= true;
        $info ="agregado";
        $data=$servicio_h;
        return responseBuilder::result($status, $info, $data);
    }
    public function agregarServicio(Request $request){
        $servicio = new Servicios();
        if($request->default){
            $servicio->default =$request->default;
        }

        $servicio->nombre =$request->nombre;
        if($servicio->save()){
            $status=true;
            $info="servicio guardado";
            $data=$servicio;
        }else{
            $status=false;
            $info="servicio no guardado";
            $data=[];
        }

        return responseBuilder::result($status, $info, $data);

    }

    public function getServiciosHotelMovil(Request $request ,$id_hotel){
        $data = Servicios_hotel::where("hotel_id",$id_hotel)->with("servicios:id,nombre")->get();

        $status=true;
        $info="servicios obtenidos";
        //print($data);
        return responseBuilder::result($status, $info, $data);


    }


    public function getServiciosHotel(Request $request ,$id_hotel){
        $data=Servicios_hotel::where("hotel_id",$id_hotel)->get();
        $status=true;
        $info="servicios obtenidos";
        //print($data);
        return responseBuilder::result($status, $info, $data);
    }

    public function archivos(Request $request,$id_hotel)
    {
       //print("entro");
        // revisar esto
       // $input = $request->all();

        //print_r($_FILES);
        $original_filename = $request->file('file')->getClientOriginalName();
        $name = $id_hotel.$original_filename;
        //$ab = $request->file("file");
       // print_r($ab);
        if ($request->file('file')->move("./hoteles/",$name)) {
            $imagen = new Imagen_hotel();
            $imagen-> imagen = "/hoteles/".$name;
            $imagen ->hotel_id =$id_hotel;
            $imagen-> save();
            return  responseBuilder::result(true, "si", "");
        }

        return  responseBuilder::result(false, "no", "");
    }

    public function editar(Request $request, $id)
    {
        // revisar esto
        $input = $request->all();
        Hotel::where('id', $id)->update($input);
            $status =True;
            $info = 'Su hotel a sido modificado correctamente ';

        return responseBuilder::result($status, $info);


    }
    public function getData(Request $request,$id_oferta){
            $data= Oferta::select("id","habitacion_id")
            ->with("habitacion:id,numero_camas,tipo_habitacion")->where("id","$id_oferta")->first();
            $status =True;
            $info = 'Recuperacion correcta ';

        return responseBuilder::result($status,$info, $data);


    }

    public function guardar(Request $request){

        try{
        $hotel = new Hotel();
        $cadena = $request->servicio;

        $servicios=preg_split("/,/",$cadena);
        $hotel->nombre = $request->nombre;
        $hotel->direccion = $request->direccion;
        $hotel->categoria = $request->categoria;
        $hotel->tipo = $request->tipo;
        $hotel->telefono = $request->telefono;
        $hotel->descripcion=$request->descripcion;
        $hotel->entrada=$request->entrada;
        $hotel->admin_id=$request->admin_id;
        $hotel->salida=$request->salida;
        $hotel->ciudad=$request->ciudad;
        $hotel->pais_id=$request->pais_id;
        $hotel->provincia_id=$request->provincia_id;
        $hotel->imagen="";

        if($hotel->save()){
            if($servicios != "" || $servicios != null){
            for ($i = 0; $i < sizeof($servicios); $i++) {
                $servicio = new Servicios_hotel();
                $servicio->hotel_id = $hotel->id;
                $servicio->servicios_id = $servicios[$i];
                $servicio->pagado = 0;
                $servicio->save();
            }
        }

            $status =  True;
            $info = 'Su hotel a sido creado correctamente ';
            return responseBuilder::result($status, $info,$hotel);
        }
        else{
            $status =  False;
            $info = 'No se ha creado el hotel';
            return responseBuilder::result($status, $info);

        }
    }catch(Exception $e){

        $status =  False;
        $info = 'No se ha creado el hotel, error del servidor';
        return responseBuilder::result($status, $info,$e);
        //print($e);

    }

    }

    public function obtener_imagenHotel(Request $request,$id_hotel)
    {
        try{
        $data =Imagen_hotel::where('hotel_id',$id_hotel)->get();
        $status=true;
        $info="imagenes del hotel";
        return responseBuilder::result($status,$info,$data);
        }catch(Exception $ex){
            $status=false;
            $info="error en la obtencion";
            return responseBuilder::result($status,$info);
        }
    }


    public function obtener_hotel(Request $request,$id){

        $data = Hotel::where("id",$id)->first();

        $status=true;
        $info="hotel";
       // print($data->pais);
        return responseBuilder::result($status,$info,$data);

    }

    public function obtener_oferta(Request $request){
        $numero_niños = $request->numero_n;
        $numero_adultos = $request->numero_a;
        $habitaconR = $request->numero_h;
        $fechai = $request->fecha_inicial;
        $ciudad =$request->ciudad;
        $fechaf = $request->fecha_final;
        $id_pais=Paises::select("id")->where("name",$ciudad)->first();
        if ($id_pais == null){
            $id_pais=-1;
        }else{
            $id_pais =$id_pais->id;
        }
        $id_provincia=Provincia::select("id")->where("name",$ciudad)->first();
        if ($id_provincia == null){
            $id_provincia=-1;
        }else{
            $id_provincia =$id_provincia->id;
        }
        //$data =Habitacion::where("numero_adultos",'>=',$numero_adultos)
        //->where("numero_niños",'>=',$numero_niños)//->get();
        //->with("oferta")->get();
        //["reserva" ,"habitacion:id,hotel_id,numero_niños,numero_adultos","habitacion.hotel"]
        //$data = Hotel::with(["pais:id,name","provincia:id,name","habitaciones.oferta:finaliza,habitacion_id,numero_habitaciones"])->get();
       // $data =Reserva::where("fecha_inicio",">=",$fechai)->where("fecha_hasta","<=",$fechaf)->count();
        //$data = Oferta::where("numero_habitacion",">",)->first();
        $data = Hotel::with(
            ["pais:id,name",
            "provincia:id,name",
            "habitaciones"=> function($query) use($numero_niños,$numero_adultos){
                $query->select("id","hotel_id","numero_niños","numero_adultos")
                ->where("numero_niños",">=",$numero_niños)
                ->where("numero_adultos",">=",$numero_adultos)
                ;
            },
            "habitaciones.oferta"=> function($query) use($habitaconR,$fechai,$fechaf){
                $query->select("id","habitacion_id","numero_habitaciones",
                "precio","desayuno")
                ->where("numero_habitaciones",">=",$habitaconR)
                ->where("finaliza",">=",$fechaf)
                ->withCount(["reserva"=> function($query) use($fechai){
                $query->where("fecha_inicio","<=",$fechai)
                ->where("fecha_hasta",">=",$fechai);

                }]);

            }
            ])->where("ciudad",$ciudad)->orwhere("nombre","like",'%'.$ciudad.'%')
            ->orwhere("pais_id",$id_pais)
            ->orwhere("provincia_id",$id_provincia)->get();
       // $data= $data[0]->pais;

        $status=true;
        $info="hotel";
       // print($data->pais);
        return responseBuilder::result($status,$info,$data);

    }

    public function Imagenes(Request $request,$id_hotel){
            $imagenes =Imagen_hotel::select("imagen")->where("hotel_id",$id_hotel)->get();
            $status=true;
            $info="hotel";
           // print($data->pais);
            return responseBuilder::result($status,$info,$imagenes);

        }



    //
}
