<?php
namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Lumen\Auth\Authorizable;

class Paises extends Model
{
protected $table= 'modelos_country';
protected $primaryKey = 'id';
/**
* The attributes that are mass assignable.
*
* @var array
*/
protected $fillable = [
'name','name_ascii','slug','geoname_id','alternate_names','code2','code3'.'continent','tld','phone'
];


public function provincia(){
    return $this->HasMany(Provincia::class);
}


public $timestamps = false;
/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/
/**protected $hidden = [
'password',
];*/
}
