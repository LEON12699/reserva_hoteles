<?php
namespace App;


use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Imagen_hotel extends Model
{
protected $table= 'modelos_imagen_hotel';
protected $primaryKey = 'id';
/**
* The attributes that are mass assignable.
*
* @var array
*/
protected $fillable = [
'imagen','hotel_id'
];

public function hotel(){
    return $this->belongsTo(Hotel::class,"hotel_id");
}

public $timestamps = false;
/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/
/**protected $hidden = [
'password',
];*/
}
