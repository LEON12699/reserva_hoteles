<?php
namespace App;


use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Provincia extends Model
{
protected $table= 'modelos_region';
protected $primaryKey = 'id';
/**
* The attributes that are mass assignable.
*
* @var array
*/
protected $fillable = [
'name','name_ascii','slug','geoname_id','alternate_names','display_name','geoname_code'.'country_id'];



public function pais(){
    return $this->belongsTo(Paises::class,"country_id","id");
}

public $timestamps = false;
/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/
/**protected $hidden = [
'password',
];*/
}
