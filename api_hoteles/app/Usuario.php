<?php
namespace App;


use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Usuario extends Model
{
protected $table= 'modelos_usuario';
protected $primaryKey = 'id';
/**
* The attributes that are mass assignable.
*
* @var array
*/
protected $fillable = [
'nombre','apellidos','imagen','contactos','is_admin','external','pais_id'.'telefono','cuenta_id'
];

public function cuenta(){
    return $this->belongsTo(User::class);
}


public function paises(){
    return $this->belongsTo(Paises::class);
}

public function hotel(){
    return $this->hasOne(Hotel::class);
}

public function reservas(){
    return $this->hasOne(Reserva::class);
}

public $timestamps = false;
/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/
/**protected $hidden = [
'password',
];*/
}
