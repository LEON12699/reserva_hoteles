<?php
namespace App;


use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Habitacion extends Model
{
protected $table= 'modelos_descripcion_habitacion';
protected $primaryKey = 'id';
/**
* The attributes that are mass assignable.
*
* @var array
*/
protected $fillable = [
'tipo_habitacion','numero_adultos','numero_niños','descripcion','hotel_id'
];

public function hotel(){
    return $this->belongsTo(Hotel::class,"hotel_id");
}

public function oferta(){
    return $this->hasOne(Oferta::class);
}



public $timestamps = false;
/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/
/**protected $hidden = [
'password',
];*/
}
