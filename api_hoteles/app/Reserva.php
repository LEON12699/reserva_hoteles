<?php
namespace App;


use Illuminate\Database\Eloquent\Model;


class Reserva extends Model
{
protected $table= 'modelos_reserva';
protected $primaryKey = 'id';
/**
* The attributes that are mass assignable.
*
* @var array
*/
protected $fillable = [
'fecha_inicio','valor_paga','fecha_hasta','oferta_id','usuario_id','requerimientos'
];

public function oferta(){
    return $this->belongsTo(Oferta::class,'oferta_id');
}


public function usuario(){
    return $this->belongsTo(Usuario::class,"usuario_id");
}


public $timestamps = false;
/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/
/**protected $hidden = [
'password',
];*/
}
