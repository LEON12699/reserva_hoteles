<?php
namespace App;


use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Oferta extends Model
{
protected $table= 'modelos_oferta';
protected $primaryKey = 'id';
/**
* The attributes that are mass assignable.
*
* @var array
*/
protected $fillable = [
'numero_habitaciones','creada','finaliza','precio','cancelacion','desayuno','habitacion_id'
];

public function habitacion(){
    return $this->belongsTo(Habitacion::class,"habitacion_id");
}
public function reserva(){
    return $this->hasMany(Reserva::class);
}




public $timestamps = false;
/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/
/**protected $hidden = [
'password',
];*/
}
