<?php
namespace App;


use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Servicios_hotel extends Model
{
protected $table= 'modelos_servicios_hotel';
protected $primaryKey = 'id';
/**
* The attributes that are mass assignable.
*
* @var array
*/
protected $fillable = [
'pagado','hotel_id',"servicios_id",
];



public function servicios(){
    return $this->belongsTo(Servicios::class,"servicios_id");
}


public function hotel(){
    return $this->belongsTo(Hotel::class,"hotel_id");
}
##
#public function ciudad(){
#    return $this-> belongsTo(Ciudad::class):
#}

#public function provincia(){
#    return $this-> belongsTo(Provincia::class):
#}



public $timestamps = false;
/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/
/**protected $hidden = [
'password',
];*/
}
