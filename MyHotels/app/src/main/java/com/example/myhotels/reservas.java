package com.example.myhotels;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myhotels.Adapters.ReservaAdapter;
import com.example.myhotels.Adapters.ServicioAdapter;
import com.example.myhotels.Servicio.Conexion;
import com.example.myhotels.Servicio.ServicioWeb;
import com.example.myhotels.Servicio.volly_async;
import com.example.myhotels.Util.Reserva;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class reservas extends AppCompatActivity {

    private EditText ipconfig;
    private List<String[]> lista;
    private RecyclerView recycler;
    private ServicioWeb servicio = new ServicioWeb(this);
    private ReservaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        lista= new ArrayList<>();
        adapter = new ReservaAdapter(lista,this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservas);
        recycler = findViewById(R.id.itemR_recycler);
        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        int id_user = preferences.getInt("user",0);

        servicio.Reservas_usuario(id_user, new volly_async<JSONObject>() {
            @Override
            public void response(JSONObject response) {

                try {
                    JSONArray array = response.getJSONArray("data");
                    for (int i = 0; i <array.length() ; i++) {
                        String[] data = new String[4];
                        data[0]=array.getJSONObject(i).getJSONObject("oferta").getJSONObject("habitacion").getJSONObject("hotel").getString("nombre");
                        data[1]=array.getJSONObject(i).getString("fecha_inicio");
                        data[2]=array.getJSONObject(i).getString("fecha_hasta");
                        data[3]=array.getJSONObject(i).getString("valor_paga");
                        lista.add(data);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                adapter= new ReservaAdapter(lista,getApplicationContext());
                recycler.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                recycler.setAdapter(adapter);
            }
        });

        //recycler.setLayoutManager(new LinearLayoutManager(this));
        //recycler.setAdapter(adapter);




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_hotel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.cerrar_sesion:
                SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor =preferences.edit();
                editor.putBoolean("logeado",false);
                editor.putInt("user",-1);
                editor.commit();
                Intent intent = new Intent(getApplicationContext(),LoginAcitvity.class);
                startActivity(intent);
                break;
            case R.id.ip_config:
                Dialog dialogo = new Dialog(this);
                dialogo.setContentView(R.layout.ip_config);
                ipconfig = dialogo.findViewById(R.id.txt_ipconfig);
                dialogo.show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;}

    public void CambiaIp (View view){
        Conexion.ip = ipconfig.getText().toString();
        Toast.makeText(getApplicationContext(),Conexion.ip,Toast.LENGTH_LONG).show();
    }

}
