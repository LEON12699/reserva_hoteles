package com.example.myhotels;

// LOGIN ACTIVITY JAVA FILE


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myhotels.Decoration.CustomDialog;
import com.example.myhotels.Decoration.Validador;
import com.example.myhotels.Servicio.Conexion;
import com.example.myhotels.Servicio.ServicioWeb;
import com.example.myhotels.Servicio.volly_async;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoginAcitvity extends AppCompatActivity {
   // List<Pais> paises;
    EditText namer,password,ipconfig;
   // Spinner spinner;
    ServicioWeb servicio;
    Validador valida = new Validador();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_acitvity);
        password = findViewById(R.id.txt_password);
        namer = findViewById(R.id.username);
        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        if(preferences.getBoolean("logeado",false)){
            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
        }


        //ip_boton = findViewById(R.id.floating_btn);

        //View bottom_Sheet = findViewById(R.id.framelayout_bottom_sheet);
      //  spinner = findViewById(R.id.spinner);

      //  spinner.setAdapter(new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,paises));
        /*final View bottonlayout = getLayoutInflater().inflate(R.layout.dialog_rooms_persons,null);
        mbooton_shet = new BottomSheetDialog(this);
        mbooton_shet.setContentView(bottonlayout);
        mbooton_shet.show();
        */

    }

    // Registro metodo
    public void registrar(View view){
        CustomDialog dialogo = new CustomDialog();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(R.id.main_content,dialogo).addToBackStack(null).commit();


        }

    // metodo de registro
    public void cambiarIP(View view) {
         Dialog dialogo = new Dialog(this);
        dialogo.setContentView(R.layout.ip_config);
        ipconfig = dialogo.findViewById(R.id.txt_ipconfig);
        dialogo.show();
    }



    //LOGIN METHOD
    public void doLogin(View view){
        final String name = namer.getText().toString();
        String contraseña = password.getText().toString();
        final List<EditText> campos = new ArrayList<>();
        campos.add(namer);
        campos.add(password);
        view.setClickable(false);
        if(valida.Vacio(this,campos,"Existen campos vacios ")) {
            valida.bloquear(campos);

            servicio = new ServicioWeb(this);

        /*if (name=="admin") {
            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
        }*/

            servicio.logear(name, contraseña, new volly_async<JSONObject>() {
                @Override
                public void response(JSONObject response) {
                    try {
                        Log.e("datos", response.toString());
                        if (response.getBoolean("status")) {

                            if(response.getJSONObject("data").getInt("is_active")==1) {
                                Toast.makeText(getApplicationContext(), response.getString("informacion"), Toast.LENGTH_LONG).show();
                                SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putBoolean("logeado", true);
                                editor.putInt("user", response.getJSONObject("data").getJSONObject("cuenta").getInt("id"));
                                editor.commit();

                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }
                            else {
                                Toast.makeText(getBaseContext(),"Su cuenta no esta activa", Toast.LENGTH_LONG).show();
                                valida.desbloquear(campos);

                            }
                        } else {
                            Toast.makeText(getBaseContext(), response.getString("informacion"), Toast.LENGTH_LONG).show();
                            valida.desbloquear(campos);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        valida.desbloquear(campos);

                    }
                }
            });
        }
        view.setClickable(true);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_hotel, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.show_bookings:

                break;
            case R.id.ip_config:
                Dialog dialogo = new Dialog(this);
                dialogo.setContentView(R.layout.ip_config);
                ipconfig = dialogo.findViewById(R.id.txt_ipconfig);
                dialogo.show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;}

    // evento de boton para cambiar la ip
    public void CambiaIp (View view){
        Conexion.ip = ipconfig.getText().toString();
        Toast.makeText(getApplicationContext(),Conexion.ip,Toast.LENGTH_LONG).show();
    }


}


