package com.example.myhotels.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myhotels.HotelViewer;
import com.example.myhotels.R;

import java.util.List;

import com.example.myhotels.Servicio.Conexion;
import com.example.myhotels.Util.HotelView;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Ravi Tamada on 18/05/16.
 */
public class HotelsAdapter extends RecyclerView.Adapter<HotelsAdapter.MyViewHolder> {

    private Context mContext;
    private List<HotelView> hotelList;
    public HotelView hotel;
    private String fecha_inicio,fecha_hasta;
    private int habitacion;

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_hasta() {
        return fecha_hasta;
    }

    public void setFecha_hasta(String fecha_hasta) {
        this.fecha_hasta = fecha_hasta;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, location,ciudad,precio,desayuno,agotado,precio_valor,disponibles;
        public RatingBar rating;

        public ImageView thumbnail;

        public MyViewHolder(View view)  {
            super(view);

            title = (TextView) view.findViewById(R.id.txt_title);
            location = (TextView) view.findViewById(R.id.txt_item_direccion);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            rating =  view.findViewById(R.id.ratingBar2);
            desayuno = view.findViewById(R.id.txt_desayuno);
            ciudad = view.findViewById(R.id.txt_pais_ciudad);
            agotado=view.findViewById(R.id.txt_agotado);
            precio=view.findViewById(R.id.txt_precio);
            precio_valor=view.findViewById(R.id.txt_precio_valor);
            disponibles = view.findViewById(R.id.txt_item_disponibles);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
           Intent intent = new Intent(mContext, HotelViewer.class);
            Log.e("dato",getAdapterPosition()+"");

            HotelView hotel = HotelsAdapter.this.hotelList.get(getAdapterPosition());
            intent.putExtra("hotel",hotel);
            intent.putExtra("fecha_inicial",fecha_inicio);
            intent.putExtra("fecha_final",fecha_hasta);

            mContext.startActivity(intent);
            //intent.putExtra("hotel",this.itemView.h)
           //mContext.startActivity(intent);

            //   recyclerViewOnItemClickListener.onClick(v,getAdapterPosition());
        }

    }



    public HotelsAdapter(Context mContext, List<HotelView> hotelList) {
        this.mContext = mContext;
        this.hotelList = hotelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hotel_card, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        hotel = hotelList.get(position);
        holder.title.setText(hotel.getNombre());
        holder.location.setText(hotel.getDireccion());
        holder.rating.setRating(hotel.getRating());
        holder.precio_valor.setText("$ "+hotel.getPrecio()+"");
        holder.ciudad.setText(hotel.getPais()+" , "+hotel.getProvincia()+", "+hotel.getCiudad()); // cambiar
        holder.precio.setText("Precio para una noche 1 habitacion, "+hotel.getNumero_adultos()+"adultos , "+hotel.getNumero_niños()+"niños,");
        holder.disponibles.setText("Disponibles :"+hotel.getNumero_habitaciones());
        if(hotel.getDesayuno()==0){
            holder.desayuno.setVisibility(View.INVISIBLE);

        }

        if(hotel.getAgotado()==1){
            holder.agotado.setVisibility(View.VISIBLE);
            holder.itemView.setClickable(false);

            holder.itemView.setBackgroundColor(Color.argb(126,2,0,2));
        }

        //  holder.precio_valor.setText(hotel.calor());
        // loading album cover using Glide library
        if (hotel.getImagen() == "null" || hotel.getImagen().equalsIgnoreCase("")) {
            Glide.with(mContext).load(R.drawable.hotel1).into(holder.thumbnail);

        }else{
            Glide.with(mContext).load( Conexion.Getpath().concat("/").concat(hotel.getImagen())).error(R.drawable.hotel1).into(holder.thumbnail);

        }

        /*holder.viewbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, HotelViewer.class);
                intent.putExtra("hotelname",holder.title.getText().toString());
                mContext.startActivity(intent);
            }
        });*/
    }
    public void updateList(@NotNull List<HotelView> lista1){
        hotelList.clear();
        hotelList.addAll(lista1);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return hotelList.size();
    }
}