package com.example.myhotels.Util;

import java.io.Serializable;

public class HotelView implements Serializable {

    // variables de vista del hotel
    private String nombre;
    private String direccion,pais,provincia;
    private int id;
    private String imagen="";
    private String descripcion;
    private String chekin, ckeckout;
    private String contactos;

    private int rating;
    private String ciudad;

    // variables de vista de la oferta
    private int numero_habitaciones,desayuno,agotado;
    private int id_oferta;
    private float precio;
    // variables de vista de la habitacion
    private int numero_niños,numero_adultos;


    public HotelView(String nombre, String direccion, String pais, String provincia, int rating, String ciudad, int numero_habitaciones, int desayuno, int agotado, float precio, int numero_niños, int numero_adultos) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.pais = pais;
        this.provincia = provincia;

        this.rating = rating;
        this.ciudad = ciudad;
        this.numero_habitaciones = numero_habitaciones;
        this.desayuno = desayuno;
        this.agotado = agotado;
        this.precio = precio;
        this.numero_niños = numero_niños;
        this.numero_adultos = numero_adultos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getChekin() {
        return chekin;
    }

    public void setChekin(String chekin) {
        this.chekin = chekin;
    }

    public String getCkeckout() {
        return ckeckout;
    }

    public void setCkeckout(String ckeckout) {
        this.ckeckout = ckeckout;
    }

    public String getContactos() {
        return contactos;
    }

    public void setContactos(String contactos) {
        this.contactos = contactos;
    }

    public int getId_oferta() {
        return id_oferta;
    }

    public void setId_oferta(int id_oferta) {
        this.id_oferta = id_oferta;
    }

    public String getImagen() {
        return imagen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }


    public HotelView() {
    }

    public int getAgotado() {
        return agotado;
    }

    public void setAgotado(int agotado) {
        this.agotado = agotado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }



    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }


    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getNumero_habitaciones() {
        return numero_habitaciones;
    }

    public void setNumero_habitaciones(int numero_habitaciones) {
        this.numero_habitaciones = numero_habitaciones;
    }

    public int getDesayuno() {
        return desayuno;
    }

    public void setDesayuno(int desayuno) {
        this.desayuno = desayuno;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getNumero_niños() {
        return numero_niños;
    }

    public void setNumero_niños(int numero_niños) {
        this.numero_niños = numero_niños;
    }

    public int getNumero_adultos() {
        return numero_adultos;
    }

    public void setNumero_adultos(int numero_adultos) {
        this.numero_adultos = numero_adultos;
    }
}
