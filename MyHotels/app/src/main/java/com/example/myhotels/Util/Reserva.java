package com.example.myhotels.Util;

public class Reserva {
    int oferta_id,usuario_id;
    String fecha_inicio,fecha_hasta,requerimientos;


    public int getOferta_id() {
        return oferta_id;
    }

    public void setOferta_id(int oferta_id) {
        this.oferta_id = oferta_id;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_hasta() {
        return fecha_hasta;
    }

    public void setFecha_hasta(String fecha_hasta) {
        this.fecha_hasta = fecha_hasta;
    }

    public String getRequerimientos() {
        return requerimientos;
    }

    public void setRequerimientos(String requerimientos) {
        this.requerimientos = requerimientos;
    }

    public Reserva() {
    }

    public Reserva(int oferta_id, int usuario_id, String fecha_inicio, String fecha_hasta, String requerimientos) {
        this.oferta_id = oferta_id;
        this.usuario_id = usuario_id;
        this.fecha_inicio = fecha_inicio;
        this.fecha_hasta = fecha_hasta;
        this.requerimientos = requerimientos;
    }
}
