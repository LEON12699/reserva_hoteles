package com.example.myhotels;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Selection;
import android.text.format.Time;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myhotels.Decoration.Validador;
import com.example.myhotels.Servicio.Conexion;
import com.example.myhotels.Servicio.ServicioWeb;
import com.example.myhotels.Servicio.volly_async;
import com.example.myhotels.Util.Lector;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import com.example.myhotels.Adapters.HotelsAdapter;
import com.example.myhotels.Decoration.GridSpacingItemDecoration;
import com.example.myhotels.Util.Bookings;


import com.example.myhotels.Util.HotelView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;


import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    ServicioWeb servicioWeb = new ServicioWeb(this);
    private  TextView dialog_niños,dialog_adults,dialog_habitaciones;

    private EditText habitad,fechas,ciudad_b;

    private BottomSheetDialog mbooton_shet;

    private EditText ipconfig;
    //TextView rooms,child,adults;
    private RecyclerView recyclerView;
    private HotelsAdapter adapter;
    private List<HotelView> hotelList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        hotelList = new ArrayList<>();
        initCollapsingToolbar();

        recyclerView =  findViewById(R.id.recycler_view);


        adapter = new HotelsAdapter(this, hotelList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        //recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        prepareHotels();



        tomarControl();
        try {
            Glide.with(this).load(R.drawable.back).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cargarRecycler(){

        adapter.updateList(hotelList);
        recyclerView.startLayoutAnimation();

    }

    public void tomarControl(){
        habitad = findViewById(R.id.txt_search_habitacionPersonas);
        fechas= findViewById(R.id.txt_search_fechas);
        ciudad_b = findViewById(R.id.txt_search_cities);
    }

    public void selectRoomsAndPersons(View view){
        final View bottonlayout = getLayoutInflater().inflate(R.layout.dialog_rooms_persons,null);
        mbooton_shet = new BottomSheetDialog(this);
        mbooton_shet.setContentView(bottonlayout);
        mbooton_shet.show();

    }

    public void Seleccionar_habitacion(View view){
        dialog_niños = mbooton_shet.findViewById(R.id.txt_nino_dialog);
        dialog_adults =mbooton_shet.findViewById(R.id.txt_adulto_dialog);
        dialog_habitaciones=mbooton_shet.findViewById(R.id.txt_habitacion_dialog);
        String cadenaFormat = dialog_habitaciones.getText().toString()+"" +
                "-habitacion ,"+dialog_adults.getText().toString()+"" +
                "-adultos ,"+dialog_niños.getText().toString()+"-niños.";
        habitad.setText(cadenaFormat);
        mbooton_shet.hide();
    }


    public void validar_botones(Button boton, @NotNull TextView cambio, int limite, boolean defI){
        int contador = Integer.parseInt(cambio.getText().toString());
        if (defI){
            if (contador < limite) {
                contador++;
                cambio.setText(String.valueOf(contador));
                boton.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

            }else{

                boton.setTextColor(Color.LTGRAY);
            }
        }else{
            if (contador > limite) {
                contador--;
                cambio.setText(String.valueOf(contador));
                boton.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

            }else{

                boton.setTextColor(Color.LTGRAY);
            }
        }
    }


    public void Disminuir(@NotNull View view){
        switch (view.getId()){
            case R.id.btn_menos_adulto:
                dialog_adults= mbooton_shet.findViewById(R.id.txt_adulto_dialog);
                Button menos_adulto = mbooton_shet.findViewById(R.id.btn_menos_adulto);
                validar_botones(menos_adulto ,dialog_adults,1,false);
                break;
            case R.id.btn_menos_habitacion:
                dialog_habitaciones = mbooton_shet.findViewById(R.id.txt_habitacion_dialog);
                Button menos_habitacion = mbooton_shet.findViewById(R.id.btn_menos_habitacion);
                validar_botones(menos_habitacion ,dialog_habitaciones,1,false);
                break;
            case R.id.btn_menos_nino:
                dialog_niños = mbooton_shet.findViewById(R.id.txt_nino_dialog);
                Button menos_niño = mbooton_shet.findViewById(R.id.btn_menos_nino);
                validar_botones(menos_niño,dialog_niños,0,false);

                break;
        }
    }


    public  void Aumenta (@NotNull View view){
        switch (view.getId()){
            case R.id.btn_mas_adulto:
                dialog_adults= mbooton_shet.findViewById(R.id.txt_adulto_dialog);
                Button mas_adulto = mbooton_shet.findViewById(R.id.btn_mas_adulto);
                validar_botones(mas_adulto,dialog_adults,30,true);
                break;
            case R.id.btn_mas_habitacion:
                dialog_habitaciones = mbooton_shet.findViewById(R.id.txt_habitacion_dialog);
                Button mas_habitacion = mbooton_shet.findViewById(R.id.btn_mas_habitacion);
                validar_botones(mas_habitacion,dialog_habitaciones,10,true);
                break;
            case R.id.btn_mas_nino:
                dialog_niños = mbooton_shet.findViewById(R.id.txt_nino_dialog);
                Button mas_niño = mbooton_shet.findViewById(R.id.btn_mas_nino);
                validar_botones(mas_niño,dialog_niños,10,true);

                break;
        }
    }

    @SuppressLint("ResourceType")
    public void dial(View view){
        Calendar timez =Calendar.getInstance(TimeZone.getTimeZone(Time.getCurrentTimezone()));
        int aumenta =TimeZone.getTimeZone(Time.getCurrentTimezone()).getRawOffset();

        Log.e("tiempo21",aumenta+"");
        Log.e("tiempo",timez.getTimeInMillis()+"");
        Log.e("date",timez.getTime().getTime()+"");
        Log.e("timezone",timez.getTimeZone().getDisplayName());
        MaterialDatePicker.Builder<androidx.core.util.Pair<Long, Long>> a;
        a = MaterialDatePicker.Builder.dateRangePicker();
        CalendarConstraints.Builder constraint;
        constraint = new CalendarConstraints.Builder();
        a.setTitleText("Seleciona tu fecha para la reserva");

        constraint.setStart(timez.getTimeInMillis()-86400000+aumenta);
        constraint.setValidator(DateValidatorPointForward.from(timez.getTimeInMillis()-86400000+aumenta));

        CalendarConstraints cc = constraint.build();
        a.setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR);

        a.setSelection(new androidx.core.util.Pair<Long, Long>(timez.getTimeInMillis()+aumenta,timez.getTimeInMillis()+86400000+aumenta));
        a.setCalendarConstraints(cc);
        MaterialDatePicker<androidx.core.util.Pair<Long, Long>> b = a.build();
        b.show(getSupportFragmentManager(),a.toString());
        b.addOnPositiveButtonClickListener(
                new MaterialPickerOnPositiveButtonClickListener<androidx.core.util.Pair<Long, Long>>() {
                    @Override
                    public void onPositiveButtonClick(androidx.core.util.Pair<Long, Long> selection) {
                        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
                        Date dia_inicio = new Date(selection.first+86400000);
                        Date fin = new Date(selection.second+86400000);
                        String sfor = formato.format(dia_inicio);
                        String sfin = formato.format(fin);
                        fechas.setText(sfor +" a "+sfin);

                    }
                }
        );

    }


    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;

                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    private Bookings getBooked(){


        return null;
    }
    private void prepareHotels() {

  /*      int[] cover = {R.drawable.hicon1,
                R.drawable.hicon2,
                R.drawable.hicon3,
                R.drawable.hicon4};
        Random random = new Random();
       Bookings booking = getBooked();
        HotelView hotelview ;
        List<Hotel> hotels = Reader.getRestaurantList(getApplicationContext());

        if(booking==null)
        {
        for(Hotel h : hotels) {
            int idx = random.nextInt(12);
            idx = idx%4;
               // hotelview = new HotelView(h.getName(),h.getLocation(),cover[idx],h.getRating(),h.getFeats());
            //    hotelList.add(hotelview);
            }
        } else {
            List<Integer> ids = booking.getId();
            for(Hotel h : hotels) {
                int idx = random.nextInt(12);
                idx = idx%4;

                if(!ids.contains(h.getId())) {
                //    hotelview = new HotelView(h.getName(), h.getLocation(), cover[idx], h.getRating(), h.getFeats());
                //    hotelList.add(hotelview);
                }
            }
        }*/
      //  adapter.notifyDataSetChanged();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */



    /**
     * Converting dp to pixel
     */

    public void Buscar_boton(View view){

        Validador validador = new Validador();

        List<EditText> campos_busqueda = new ArrayList<>();
        campos_busqueda.add(ciudad_b);
        campos_busqueda.add(fechas);
        campos_busqueda.add(habitad);

        if (validador.Vacio(this,campos_busqueda,"Porvafor llene todos los campos")) {

            try {

                JSONObject numero = validador.desconstruye(habitad.getText().toString());
                JSONObject fechas_json = validador.fechas(fechas.getText().toString());
                adapter.setFecha_inicio(fechas_json.getString("fechaInicial"));
                adapter.setFecha_hasta(fechas_json.getString("fecha_fin"));
                servicioWeb.Busqueda(numero.getString("habitaciones"), numero.getString("adultos"),
                        numero.getString("niños"), fechas_json.getString("fechaInicial"),
                        fechas_json.getString("fecha_fin"), ciudad_b.getText().toString(),
                        new volly_async<JSONObject>() {
                            @Override
                            public void response(JSONObject response) {
                                hotelList=Lector.leer_busqueda(response);
                                Log.e("hotel",hotelList.size()+"");
                                cargarRecycler();

                            }
                        });
            } catch (JSONException e) {
                e.printStackTrace();

            }


        }

    }



    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    @Override
    public void onBackPressed() {
        finishAffinity();

        System.exit(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_hotel, menu);
        return true;
    }

    // evento de boton para cambiar la ip

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.show_bookings:
                Intent intent = new Intent(getApplicationContext(),reservas.class);
                startActivity(intent);
                break;
            case R.id.cerrar_sesion:
                SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor =preferences.edit();
                editor.putBoolean("logeado",false);
                editor.putInt("user",-1);
                editor.commit();
                Intent intent2 = new Intent(getApplicationContext(),LoginAcitvity.class);
                startActivity(intent2);
                break;
            case R.id.ip_config:
                Dialog dialogo = new Dialog(this);
                dialogo.setContentView(R.layout.ip_config);
                ipconfig = dialogo.findViewById(R.id.txt_ipconfig);
                dialogo.show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;}


    public void CambiaIp (View view){
        Conexion.ip = ipconfig.getText().toString();
        Toast.makeText(getApplicationContext(),Conexion.ip,Toast.LENGTH_LONG).show();
    }
}

