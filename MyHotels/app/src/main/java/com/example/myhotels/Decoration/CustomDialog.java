package com.example.myhotels.Decoration;

import android.app.Dialog;
import android.os.Bundle;
import android.transition.Transition;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.myhotels.R;
import com.example.myhotels.Servicio.Conexion;
import com.example.myhotels.Servicio.ServicioWeb;
import com.example.myhotels.Servicio.volly_async;
import com.example.myhotels.Util.Pais;
import com.example.myhotels.Util.UserAccount;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CustomDialog extends DialogFragment {

    ServicioWeb servicio;
    EditText username_login,password_login;
    EditText nombre,apellido,email,contraseña,username,telefono;
    Spinner pais;
    Validador valida = new Validador();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.dialog_registro, container, false);
        nombre = vista.findViewById(R.id.txt_registro_nombre);
        apellido = vista.findViewById(R.id.txtregistro_apellido);
        username = vista.findViewById(R.id.txt_registro_username);
        telefono = vista.findViewById(R.id.txt_registro_telefono);
        email = vista.findViewById(R.id.txt_registro_email);
        contraseña = vista.findViewById(R.id.txt_registro_password);
        pais = vista.findViewById(R.id.spinner_paises);
        final List<String> paises = new ArrayList<>();
        servicio = new ServicioWeb(getContext());

        servicio.obtenerPaises(new volly_async<JSONObject>() {
            @Override
            public void response(JSONObject response) {
                JSONArray array = null;
                try {
                    array = response.getJSONArray("data");
                    Log.e("Array", array.length() + "");
                    for (int i = 0; i < array.length(); i++) {

                        paises.add(array.getJSONObject(i).getString("name"));

                    }
                    pais.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, paises));

                    Log.e("data", response.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        username_login = (getActivity().findViewById(R.id.username));
        password_login = getActivity().findViewById(R.id.txt_password);
        (vista.findViewById(R.id.btn_registro)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setClickable(false);
                UserAccount cuenta = new UserAccount();
                final List<EditText> lista = new ArrayList<>();
                lista.add(email);
                lista.add(username);
                lista.add(apellido);
                lista.add(nombre);
                lista.add(contraseña);
                lista.add(telefono);
                if (valida.Vacio(getContext(),lista,"No deje campos vacios porfavor")) {
                   valida.bloquear(lista);
                    servicio = new ServicioWeb(getContext());

                    cuenta.setEmail(email.getText().toString());
                    cuenta.setUsername(username.getText().toString());
                    cuenta.setLast_name(apellido.getText().toString());
                    cuenta.setName(nombre.getText().toString());
                    cuenta.setPassword(contraseña.getText().toString());
                    cuenta.setTelefono(telefono.getText().toString());
                    cuenta.setPais(pais.getSelectedItem().toString());
                    //cuenta.setPais("Ecuador");

                    servicio.Registar(cuenta, new volly_async<JSONObject>() {
                        @Override
                        public void response(JSONObject response) {
                            try {
                                if (response.getBoolean("status")) {
                                    crearToast(response.getString("informacion"));
                                    JSONObject datos = response.getJSONObject("data");
                                    username_login.setText(datos.getString("contactos"));
                                    password_login.setText(contraseña.getText().toString());
                                    dismiss();

                                } else {
                                    crearToast(response.getString("informacion"));
                                    valida.desbloquear(lista);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                valida.desbloquear(lista);
                            }
                            valida.desbloquear(lista);

                        }
                    });
                }
                v.setClickable(true);

            }
        });

        (vista.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
            }
        });

        //return super.onCreateView(inflater, container, savedInstanceState);
  return vista;  }




    private void crearToast(String texto){
        Toast.makeText(getContext(),texto,Toast.LENGTH_LONG).show();
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog =super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_SWIPE_TO_DISMISS);
        return  dialog;
        //return super.onCreateDialog(savedInstanceState);
    }
}
