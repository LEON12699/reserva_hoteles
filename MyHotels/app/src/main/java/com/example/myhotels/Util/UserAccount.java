package com.example.myhotels.Util;

public class UserAccount {
    private String password, name, last_name,email,username,telefono;
    private String pais;

    public UserAccount() {
    }

    public UserAccount(String password, String name, String last_name, String email, String username, String telefono, String pais) {
        this.password = password;
        this.name = name;
        this.last_name = last_name;
        this.email = email;
        this.username = username;
        this.telefono = telefono;
        this.pais = pais;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }
}
