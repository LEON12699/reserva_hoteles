package com.example.myhotels.Util;

public class Pais {

    private String name;

    public Pais() {
    }

    public Pais( String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
