package com.example.myhotels;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myhotels.Adapters.ServicioAdapter;
import com.example.myhotels.Decoration.Validador;
import com.example.myhotels.Servicio.Conexion;
import com.example.myhotels.Servicio.ServicioWeb;
import com.example.myhotels.Servicio.volly_async;
import com.example.myhotels.Util.Reserva;
import com.example.myhotels.Util.Servicio;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.IndicatorView.draw.controller.DrawController;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

import com.example.myhotels.Adapters.SliderAdapter;
import com.example.myhotels.Util.Hotel;
import com.example.myhotels.Util.HotelView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HotelViewer extends AppCompatActivity {

    private EditText ipconfig;

    public ServicioWeb servicio= new ServicioWeb(this);




    public List<Hotel> hotels;
    public HotelView hotel = null;
    SliderView sliderView;
    private List<HotelView> hotelList = new ArrayList<>();
    private RecyclerView recyclerView;
   // private Rec_HotelsAdapter mAdapter;
    private List<String> imagenes= new ArrayList<>() ;
    private String fecha_inicial,fecha_final;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_viewer);
        Intent intent = getIntent();
       // hotelName = intent.getStringExtra("hotelname");
        hotel = (HotelView) intent.getSerializableExtra("hotel");
        fecha_inicial = intent.getStringExtra("fecha_inicial");
        fecha_final = intent.getStringExtra("fecha_final");


        Log.e("name",hotel.toString()+" "+hotel.getId());
        sliderView = findViewById(R.id.imageSlider);
        servicio.ObtenerImagenes(hotel.getId() ,new volly_async<JSONObject>() {
            @Override
            public void response(JSONObject response) {
                try {
                    JSONArray data = response.getJSONArray("data");
                    for (int i = 0; i < data.length(); i++) {
                        imagenes.add(Conexion.Getpath()+data.getJSONObject(i).getString("imagen"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                cargar_slider();
            }
        });




        setValues(hotel, this);

        //Initializing Recommendation RecyclerView
  /*      recyclerView = (RecyclerView) findViewById(R.id.rec_recycler_view);
        mAdapter = new Rec_HotelsAdapter(getApplicationContext(), hotelList);
//        RecyclerView.LayoutManager mLayoutManager =
//                new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        prepareHotelData();
*/


    }

private void cargar_slider(){
    final SliderAdapter adapter = new SliderAdapter(this,imagenes);
    Log.e("log",imagenes.size()+"");
    adapter.setCount(imagenes.size());
    //Initializing Image Slider
    sliderView.setSliderAdapter(adapter);
    sliderView.setIndicatorAnimation(IndicatorAnimations.SLIDE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
    sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
    sliderView.setAutoCycleDirection(SliderView.DRAG_FLAG_GLOBAL);
    sliderView.setIndicatorSelectedColor(Color.WHITE);
    sliderView.setIndicatorUnselectedColor(Color.GRAY);
    sliderView.setOnIndicatorClickListener(new DrawController.ClickListener() {
        @Override
        public void onIndicatorClicked(int position) {
            sliderView.setCurrentPagePosition(position);
        }
    });
}
   /* private Bookings getBooked(){
         returns booked hotel for the user
        ArrayList<Bookings> bookings = Reader.getBookingsList(getApplicationContext());
        if(bookings == null)
            return null;
        for(Bookings book : bookings){
            String name = book.getName();
            if(name == null) continue;

            if(name.equals(CurrentUser.username))
            {

                return book;

            }
        }

        return null;
    }*/
    public void prepareHotelData(){
        /*PREPARE HOTEL LIST FOR RECOMMENDING TO USER BASED ON ITS PREVIOUS BOOKINGS*/
   /*     int[] cover = {R.drawable.hicon1,
                R.drawable.hicon2,
                R.drawable.hicon3,
                R.drawable.hicon4};
        TextView title = findViewById(R.id.View_title);
        TextView feats = findViewById(R.id.View_features);
        String[] feat_text = feats.getText().toString().split(": ");
        String[] curr_feats = feat_text[1].split(" , ");

        TextView locs = findViewById(R.id.View_location);
        String[] locs_text = locs.getText().toString().split(": ");
        Random random = new Random();
        Bookings booking = getBooked();
        HotelView hotelview ;
        List<Hotel> hotels = Reader.getRestaurantList(getApplicationContext());

        if(booking==null)
        {
            for(Hotel h : hotels) {
                int idx = random.nextInt(12);
                idx = idx%4;
                if(h.getLocation().equalsIgnoreCase(locs_text[1]) && !h.getName().equalsIgnoreCase(title.getText().toString())) {
                    hotelview = new HotelView(h.getName(), h.getLocation(), cover[idx], h.getRating(), h.getFeats());
                    hotelList.add(hotelview);
                }
            }
        } else {
            List<Integer> ids = booking.getId();
            for(Hotel h : hotels) {
                int idx = random.nextInt(12);
                idx = idx%4;

                if(!ids.contains(h.getId())) {
                    if((h.getFeatures().contains(curr_feats[0]) || h.getFeatures().contains(curr_feats[1]))
                            && (!h.getName().equalsIgnoreCase(title.getText().toString()))) {
                        hotelview = new HotelView(h.getName(), h.getLocation(), cover[idx], h.getRating(), h.getFeats());
                        hotelList.add(hotelview);
                    }
                }
            }
        }
        mAdapter.notifyDataSetChanged();*/
    }

    public void setValues(HotelView hotelName, Context context){
        TextView view_titulo,descripcion,pais,ciudad,contacto,direccion,entrada,salida,reserva_adultos,
                reserva_niños,disponibles,desayuno,precio;
        RatingBar rating;
        final TextView camas=findViewById(R.id.txt_reserva_camas);
        final TextView tipoH = findViewById(R.id.txt_item_tipoHabitacion);

        servicio.getHabitacionData(hotelName.getId_oferta(), new volly_async<JSONObject>() {
                    @Override
                    public void response(JSONObject response) {
                        try {
                            Log.e("respinse",response.toString());
                            camas.setText("Camas :"+response.getJSONObject("data").getJSONObject("habitacion").getInt("numero_camas"));
                            tipoH.setText(response.getJSONObject("data").getJSONObject("habitacion").getString("tipo_habitacion"));

                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

                rating = findViewById(R.id.txt_view_rating);
                rating.setRating(hotelName.getRating());
                view_titulo = findViewById(R.id.View_title);
                view_titulo.setText(hotelName.getNombre());
                descripcion = findViewById(R.id.txt_view_descripcion);
                descripcion.setText(hotelName.getDescripcion());
                pais = findViewById(R.id.txt_view_pais);
                pais.setText(hotelName.getPais());
                ciudad=findViewById(R.id.txt_view_ciudad);
                ciudad.setText(hotelName.getCiudad());
                contacto = findViewById(R.id.txt_view_contacto);
                contacto.setText("Contacto :     "+hotelName.getContactos());
                direccion = findViewById(R.id.txt_view_direcion);
                direccion.setText("Nos encontramos :  "+hotelName.getDireccion());
                entrada = findViewById(R.id.txt_view_hentrada);
                entrada.setText(hotel.getChekin().substring(0,9));
                salida= findViewById(R.id.txt_view_hsalida);
                salida.setText(hotel.getCkeckout().substring(0,9));
                reserva_adultos = findViewById(R.id.txt_reserva_adultos);
                reserva_adultos.setText(hotelName.getNumero_adultos()+"");
                reserva_niños =findViewById(R.id.txt_reserva_niños);
                reserva_niños.setText(hotelName.getNumero_niños()+"");


                disponibles=findViewById(R.id.txt_reserva_disponible);
                disponibles.setText("Habitaciones Disponibles :"+hotelName.getNumero_habitaciones());
                desayuno=findViewById(R.id.txt_reserva_desayuno);
                if (hotelName.getDesayuno()==0){
                    desayuno.setText("NO incluyen desayuno");
                    desayuno.setTextColor(Color.RED);
                }
                precio=findViewById(R.id.txt_reserva_precio);
                precio.setText("USD$ "+ hotelName.getPrecio()+"");


        /*SETTING VALUES TO THE LAYOUT COMPONENTS
        TextView viewtitle , viewlocation , viewrating , viewfeature , viewcontact;
        final Button viewbook,viewsave;
        viewtitle = findViewById(R.id.View_title);
        viewlocation = findViewById(R.id.View_location);
        viewrating = findViewById(R.id.View_rating);
        viewfeature = findViewById(R.id.View_features);
        viewcontact = findViewById(R.id.View_contact);
        viewbook = findViewById(R.id.View_book);
        viewsave = findViewById(R.id.View_save);

        hotels = Reader.getRestaurantList(context);

        for(Hotel hot : hotels) {
            if(hot.getName().equalsIgnoreCase(hotelName)) {
                hotel = hot;
                break;
            }
        }
        viewtitle.setText(hotel.getName());
        viewlocation.setText("Locations: "+ hotel.getLocation());
        viewfeature.setText("Features: "+hotel.getFeats());
        viewrating.setText("User Rating: "+hotel.getRating());
        viewcontact.setText("Contact: "+hotel.getContact());




        // ON CLICK LISTNER FOR BOOKIING BUTTON
        viewbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String stat = viewbook.getText().toString();
                if(stat.equalsIgnoreCase("book"))
                {
                    viewbook.setText("Booked");
                    int index = -1;
                    ArrayList<Bookings> bookings = Reader.getBookingsList(getApplicationContext());
                    Bookings firstBooking = null;
                    if(bookings == null) //CHECK IF NO BOOKING HAS BEEN DONE BY ANY USER
                    {
                        firstBooking = new Bookings();
                        firstBooking.setName(CurrentUser.username);
                        List<Integer> ids = new ArrayList<>();
                        ids.add(hotel.getId());
                        firstBooking.setId(ids);
                        bookings = new ArrayList<>();
                        bookings.add(firstBooking);
                        Writer.writeBookings(getApplicationContext(), bookings);
                    }
                    else {
                        boolean status = false;
                        for (Bookings book : bookings) {
                            //FINDING ALL THE BOOKINGS FOR THE CURRENTUSER
                            if(book.getName() == null) continue;
                            if (book.getName().equalsIgnoreCase(CurrentUser.username)) {
                                status = true;
                                firstBooking = book;
                                index = bookings.indexOf(book);

                                break;
                            }
                        }
                        if (status) {
                            //USER ALREADY HAS SOME BOOKING
                            List<Integer> ids = firstBooking.getId();
                            if(ids.contains(hotel.getId()))
                                return;
                            ids.add(hotel.getId());
                            bookings.remove(index);
                            firstBooking.setId(ids);
                            bookings.add(firstBooking);

                        } else {
                            //USER BOOKING FOR FIRST TIME
                            firstBooking = new Bookings();
                            firstBooking.setName(CurrentUser.username);
                            List<Integer> ids = new ArrayList<>();
                            ids.add(hotel.getId());
                            firstBooking.setId(ids);
                            bookings.add(firstBooking);
                        }
                        Writer.writeBookings(getApplicationContext(), bookings);
                    }

                }
                else
                        Toast.makeText(getApplicationContext(),"Already Booked",Toast.LENGTH_LONG).show();
            }
        });
        viewsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String stat = viewsave.getText().toString();
                if(stat.equalsIgnoreCase("Save for later"))
                {
                    viewsave.setText("Saved");
                    int index = -1;
                    ArrayList<Drafts> drafts = Reader.getDraftsList(getApplicationContext());
                    Drafts firstDraft = null;
                    if(drafts == null){
                        firstDraft = new Drafts();
                        firstDraft.setName(CurrentUser.username);
                        List<Integer> ids = new ArrayList<>();
                        ids.add(hotel.getId());
                        firstDraft.setId(ids);
                        drafts = new ArrayList<>();
                        drafts.add(firstDraft);
                        Writer.writeDrafts(getApplicationContext(), drafts);
                    }
                    else {
                        boolean status = false;
                        for (Drafts book : drafts) {
                            if (book.getName().equalsIgnoreCase(CurrentUser.username)) {
                                status = true;
                                firstDraft = book;
                                index = drafts.indexOf(book);
                                //Toast.makeText(getApplicationContext(),book.getId().size(),Toast.LENGTH_LONG).show();
                                break;
                            }
                        }
                        if (status) {
                            List<Integer> ids = firstDraft.getId();
                            if(ids.contains(hotel.getId()))
                                return;
                            ids.add(hotel.getId());
                            drafts.remove(index);
                            firstDraft.setId(ids);
                            drafts.add(firstDraft);

                        } else {
                            firstDraft = new Drafts();
                            firstDraft.setName(CurrentUser.username);
                            List<Integer> ids = new ArrayList<>();
                            ids.add(hotel.getId());
                            firstDraft.setId(ids);
                            drafts.add(firstDraft);
                        }
                        Writer.writeDrafts(getApplicationContext(), drafts);
                    }
                }
                else
                    Toast.makeText(getApplicationContext(),"Already Saved",Toast.LENGTH_LONG).show();
            }
        });
    */}



    public void btn_Reservar(View view){
        final Dialog dialogo = new Dialog(this);
        dialogo.setContentView(R.layout.dialog_reservar);
        dialogo.setTitle("servicios");
        final Validador validador = new Validador();
        final EditText numero,requerimientos;
        requerimientos = dialogo.findViewById(R.id.txt_reserva_requerimientos);
        numero= dialogo.findViewById(R.id.txt_confirmar_numero);
        dialogo.findViewById(R.id.btn_reservar_completo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(hotel.getNumero_habitaciones()>= Integer.parseInt(numero.getText().toString())
                 && validador.Vacio(getBaseContext(),numero,"Porfavor llene el numero de habitaciones para su reserva")) {
                    SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
                    int dato =preferences.getInt("user", 0);
                    if (dato!=0 ) {
                        Log.e("dar",Integer.parseInt(numero.getText().toString())+"d");
                        for (int i = 0; i < Integer.parseInt(numero.getText().toString()); i++) {
                            Reserva r = new Reserva();
                            r.setFecha_hasta(fecha_final);
                            r.setFecha_inicio(fecha_inicial);
                            r.setOferta_id(hotel.getId_oferta());
                            r.setUsuario_id(dato);
                            r.setRequerimientos(requerimientos.getText().toString());
                            Log.e("dato",r.getFecha_inicio()+r.getFecha_hasta()+r.getUsuario_id());
                            servicio.Reservando(r, new volly_async<JSONObject>() {
                                @Override
                                public void response(JSONObject response) {

                                            Toast.makeText(getApplicationContext(),"Reserva exitosa",Toast.LENGTH_LONG).show();
                                            Intent intent = new Intent(getApplicationContext(),reservas.class);
                                            startActivity(intent);


                                }
                            });
                        }
                     }
                }else{
                    Toast.makeText(getApplicationContext(),"El numero ingresado es mayor al disponible",Toast.LENGTH_LONG).show();
                }

            }
        });
        dialogo.show();

    }


    public void btn_VerServicios(View view){
        final List<Servicio> servicios = new ArrayList<>();
        final Dialog a = new Dialog(this);
            a.setContentView(R.layout.dialog_servicios);
            a.setTitle("servicios");

            final RecyclerView recycler = a.findViewById(R.id.recycler_servicios);
        servicio.ObtenerServicios(hotel.getId(), new volly_async<JSONObject>() {
            @Override
            public void response(JSONObject response) {

                try {
                    JSONArray array = response.getJSONArray("data");
                    for (int i = 0; i < array.length(); i++) {
                        Servicio ser = new Servicio();
                        ser.setPago(array.getJSONObject(i).getInt("pagado"));
                        ser.setNombre(array.getJSONObject(i).getJSONObject("servicios").getString("nombre"));
                        servicios.add(ser);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ServicioAdapter Adapter = new ServicioAdapter(getApplicationContext(),servicios);
                recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

                recycler.setAdapter(Adapter);
                a.show();
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_hotel, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.show_bookings:
                Intent intent = new Intent(getApplicationContext(),reservas.class);
                startActivity(intent);
                break;
            case R.id.cerrar_sesion:
                SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor =preferences.edit();
                editor.putBoolean("logeado",false);
                editor.putInt("user",-1);
                editor.commit();
                Intent intent2 = new Intent(getApplicationContext(),LoginAcitvity.class);
                startActivity(intent2);
                break;
            case R.id.ip_config:
                Dialog dialogo = new Dialog(this);
                dialogo.setContentView(R.layout.ip_config);
                ipconfig = dialogo.findViewById(R.id.txt_ipconfig);
                dialogo.show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;}

    // evento de boton para cambiar la ip
    public void CambiaIp (View view){
        Conexion.ip = ipconfig.getText().toString();
        Toast.makeText(getApplicationContext(),Conexion.ip,Toast.LENGTH_LONG).show();
    }

   /* @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }*/
}