package com.example.myhotels.Decoration;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myhotels.R;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Validador {

    public Validador() {
    }

    public boolean Vacio(Context context,EditText campo, String mensaje){
        if(campo.getText().toString()==""){
            Toast.makeText(context,mensaje,Toast.LENGTH_LONG).show();
            return false;
        }
    return true;}

    public boolean Vacio(Context context, List<EditText> campo, String mensaje){

        for (EditText a:campo) {
            Log.e("a",a.getText().toString()+"s");
            if(a.getText().toString().length()<1){
                Toast.makeText(context,mensaje,Toast.LENGTH_LONG).show();
                return false;
            }
            }
        return true;}

    public void bloquear( List<EditText> campo){

        for (EditText a:campo) {
            a.setFocusable(false);
            a.setMaxLines(1);

            a.setTextColor(Color.LTGRAY);
            }
        }

    public void desbloquear( List<EditText> campo){

        for (EditText a:campo) {
            a.setTextColor(Color.BLACK);
            a.setFocusableInTouchMode(true);
            a.setFocusable(true);
            a.setMaxLines(1);
        }
    }


    public JSONObject desconstruye(String cadena) throws JSONException {
        String[] data =cadena.replace(" ","").split(",");
        JSONObject atributos = new JSONObject();
        atributos.put("niños",data[0].split("-")[0]);
        atributos.put("adultos",data[1].split("-")[0]);
        atributos.put("habitaciones",data[2].split("-")[0]);

        return  atributos;


    }

    public JSONObject fechas(String cadena) throws JSONException {
        String[] data =cadena.replace(" ","").split("a");
        JSONObject atributos = new JSONObject();
        String [] fecha_init = data[0].split("-");
        String [] fecha_fin = data[1].split("-");

        atributos.put("fechaInicial",fecha_init[2]+"-"+fecha_init[1]+"-"+fecha_init[0]);
        atributos.put("fecha_fin",fecha_fin[2]+"-"+fecha_fin[1]+"-"+fecha_fin[0]);

        return  atributos;


    }

    public boolean ingreso_habitaciones(String numero, String disponibles){
    return Integer.parseInt(numero) <= Integer.parseInt(disponibles);
    }
}





