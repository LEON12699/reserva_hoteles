package com.example.myhotels.Servicio;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class SingletonQuee {
    private RequestQueue queue;
    private Context context;
    private static SingletonQuee miInstancia;

    public SingletonQuee(Context context) {
        this.context = context;
        queue = getRequestQuee();
    }

    private RequestQueue getRequestQuee() {
        if(queue == null) {
            queue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return queue;
    }

    public <T> void addToRequestQueue(Request request){
        queue.add(request);
    }



    public static synchronized SingletonQuee getInstance(Context context){
        if(miInstancia==null){
            miInstancia = new SingletonQuee(context);
        }

        return miInstancia;}


}
