package com.example.myhotels.Adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.myhotels.R;
import com.example.myhotels.Servicio.Conexion;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterVH> {

    private Context context;
    //private int mCount;
    private List<String> imagenes;
    private int count = 0;

    public SliderAdapter(Context context, List<String> imagenes) {
        this.context = context;
        this.imagenes = imagenes;

    }

    public void  update(List<String> imagenes){
        this.imagenes.clear();
        this.imagenes=imagenes;
        notifyDataSetChanged();
    }

    public void agregar(String imagen){
        this.imagenes.add(imagen);
        notifyDataSetChanged();
    }

    public SliderAdapter(Context context) {
        this.context = context;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {
        String url = imagenes.get(position);

        Glide.with(viewHolder.itemView)
        .load(url)
                .fitCenter()
                .error(R.drawable.hotel2)
                .placeholder(R.drawable.hotel1)
                .into(viewHolder.imageViewBackground);

    /*
        switch (position) {
            case 0:
                Glide.with(viewHolder.itemView)
                        .load(imagenes.get(i))
                        .fitCenter()
                        .into(viewHolder.imageViewBackground);
                break;
            case 1:
//
                Glide.with(viewHolder.itemView)
                        .load(R.drawable.hotel1)
                        .fitCenter()
                        .into(viewHolder.imageViewBackground);
                break;



        }
*/
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return count;
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        ImageView imageGifContainer;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);

            this.itemView = itemView;
        }
    }


}