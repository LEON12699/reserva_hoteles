package com.example.myhotels.Servicio;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.myhotels.R;
import com.example.myhotels.Util.Reserva;
import com.example.myhotels.Util.UserAccount;

import org.json.JSONException;
import org.json.JSONObject;

public class ServicioWeb {

    Context context;

    String registro="/cuenta";
    String logear="/cuenta/login";
    String getPaises="/paises";
    String getBusqueda="/hotel/obtener_oferta";
    String getImagenes="/hotel/imagenes";
    String getServicios="/hotel/serviciosMovil";
    String postReservar="/reservas";
    String getData= "/hotel/get_data_movil";
    String reservasUsuario="/reservas/usuario";

    public ServicioWeb( Context context) {
        this.context = context;
    }


    public void getHabitacionData(int id_oferta,final volly_async<JSONObject> voly){
        String path =Conexion.Getpath().concat(getData).concat("/"+id_oferta);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path, null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();
                voly.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Haga bien joven",Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        SingletonQuee.getInstance(context).addToRequestQueue(request);

    }


    public void Reservas_usuario(int id_usuario,final volly_async<JSONObject> voly){
        String path =Conexion.Getpath().concat(reservasUsuario).concat("/"+id_usuario);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path, null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();
                voly.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Haga bien joven",Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        SingletonQuee.getInstance(context).addToRequestQueue(request);

    }




    public void obtenerPaises(final volly_async<JSONObject> voly){
        String path =Conexion.Getpath().concat(getPaises);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path, null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();
                voly.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Haga bien joven",Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        SingletonQuee.getInstance(context).addToRequestQueue(request);

    }

    public void Registar(UserAccount usuario, final volly_async<JSONObject> voly){
        String path =Conexion.Getpath().concat(registro);
        JSONObject json = new JSONObject() ;
        try {
            json.put("username", usuario.getUsername());
            json.put("password", usuario.getPassword());
            json.put("nombre", usuario.getLast_name());
            json.put("apellidos", usuario.getName());
            json.put("is_superuser", 0);
            json.put("email", usuario.getEmail());
            json.put("pais", usuario.getPais());
            json.put("telefono", usuario.getTelefono());
            json.put("hotel_admin", 0);
        }catch (Exception ex){
            ex.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();
                voly.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Haga bien joven",Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        SingletonQuee.getInstance(context).addToRequestQueue(request);
    }


    public void logear(String username,String contraseña,final volly_async<JSONObject> voly){
        String path =Conexion.Getpath().concat(logear);
        JSONObject json = new JSONObject() ;
        try {
            json.put("username", username);
            json.put("password", contraseña);

        }catch (Exception ex){
            ex.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();

                voly.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Haga bien joven",Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        SingletonQuee.getInstance(context).addToRequestQueue(request);

    }

    public void ObtenerServicios(int hotel,final volly_async<JSONObject> voly){
        String path =Conexion.Getpath().concat(getServicios).
                concat("/"+hotel);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path, null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();

                voly.response(response);
                // Log.e("e",response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Haga bien joven",Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        SingletonQuee.getInstance(context).addToRequestQueue(request);

    }



    public void ObtenerImagenes(int hotel,final volly_async<JSONObject> voly){
        String path =Conexion.Getpath().concat(getImagenes).
                concat("/"+hotel);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path, null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();

                voly.response(response);
                // Log.e("e",response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Haga bien joven",Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        SingletonQuee.getInstance(context).addToRequestQueue(request);

    }



    public void Reservando(Reserva reservar, final volly_async<JSONObject> voly){
        final Dialog dialogo = new Dialog(context);
        dialogo.setTitle("Progreso..");
        dialogo.setContentView(R.layout.dialog_progress);
        dialogo.show();
        String path =Conexion.Getpath().concat(postReservar);
        JSONObject json = new JSONObject();

        try {
            json.put("fecha_hasta",reservar.getFecha_hasta());
            json.put("fecha_inicio",reservar.getFecha_inicio());
            json.put("oferta_id",reservar.getOferta_id());
            json.put("requerimientos",reservar.getRequerimientos());
            json.put("usuario_id",reservar.getUsuario_id());
            Log.e("data",json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();
                voly.response(response);
                Log.e("e",response.toString());
                dialogo.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context,"Haga bien joven",Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        SingletonQuee.getInstance(context).addToRequestQueue(request);

    }



    public void Busqueda(String numero_n,String numero_a,String numero_h,String fecha_inicial,String fecha_final, String ciudad,final volly_async<JSONObject> voly){
        final Dialog dialogo = new Dialog(context);
        dialogo.setTitle("Progreso..");
        dialogo.setContentView(R.layout.dialog_progress);
        dialogo.show();
        String path =Conexion.Getpath().concat(getBusqueda).
                concat("?numero_n="+numero_n).
                concat("&numero_a="+numero_a)
                .concat("&numero_h="+numero_h)
                .concat("&fecha_inicial="+fecha_inicial)
                .concat("&fecha_final="+fecha_final)
                .concat("&ciudad="+ciudad);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path, null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();

                voly.response(response);
                Log.e("e",response.toString());
                dialogo.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Haga bien joven",Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        SingletonQuee.getInstance(context).addToRequestQueue(request);

    }



}
