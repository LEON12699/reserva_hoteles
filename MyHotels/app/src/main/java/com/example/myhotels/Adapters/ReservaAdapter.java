package com.example.myhotels.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myhotels.R;

import java.util.List;

public class ReservaAdapter extends RecyclerView.Adapter<ReservaAdapter.ViewHolder> {
    List<String[]> lista;
    Context context;

    public ReservaAdapter(List<String[]> lista, Context context) {
        this.lista = lista;
        this.context = context;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_reservas, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            String[] datos = lista.get(position);
            holder.hotel.setText(datos[0]);
            holder.fechas.setText(datos[1] + " a "+datos[2]);
            holder.precio.setText(""+datos[3]);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView hotel,precio,fechas;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            hotel = itemView.findViewById(R.id.txt_item_hotelR);
            precio=itemView.findViewById(R.id.txt_item_valorR);
            fechas=itemView.findViewById(R.id.txt_item_fechasR);

        }
    }
}
