package com.example.myhotels.Util;

import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public  class Lector {
    @NotNull
    public static List<HotelView> leer_busqueda(JSONObject objeto) {
        List<HotelView> lista = new ArrayList<>();
        Log.e("respuesta",objeto
        .toString());
            try {
                JSONArray data = objeto.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONArray habitaciones = data.getJSONObject(i).getJSONArray("habitaciones");
                    for (int k = 0; k < habitaciones.length(); k++) {
                        HotelView vista = new HotelView();
                        vista.setId(data.getJSONObject(i).getInt("id"));
                        vista.setImagen(data.getJSONObject(i).getString("imagen"));
                        vista.setNombre(data.getJSONObject(i).getString("nombre"));
                        vista.setRating(data.getJSONObject(i).getInt("categoria"));
                        vista.setDireccion(data.getJSONObject(i).getString("direccion"));
                        vista.setCiudad(data.getJSONObject(i).getString("ciudad"));
                        vista.setPais(data.getJSONObject(i).getJSONObject("pais").getString("name"));
                        vista.setProvincia(data.getJSONObject(i).getJSONObject("provincia").getString("name"));
                        vista.setDescripcion(data.getJSONObject(i).getString("descripcion"));
                        vista.setChekin(data.getJSONObject(i).getString("entrada"));
                        vista.setCkeckout(data.getJSONObject(i).getString("salida"));
                        vista.setContactos(data.getJSONObject(i).getString("telefono"));

                        vista.setNumero_niños(habitaciones.getJSONObject(k).getInt("numero_niños"));
                        vista.setNumero_adultos(habitaciones.getJSONObject(k).getInt("numero_adultos"));
                        int habitaciones_antes = (habitaciones.getJSONObject(k).getJSONObject("oferta").getInt("numero_habitaciones"));
                        vista.setPrecio(Float.parseFloat(habitaciones.getJSONObject(k).getJSONObject("oferta").getString("precio")));
                        vista.setDesayuno(habitaciones.getJSONObject(k).getJSONObject("oferta").getInt("desayuno"));
                        vista.setId_oferta(habitaciones.getJSONObject(k).getJSONObject("oferta").getInt("id"));
                        int menos = habitaciones.getJSONObject(k).getJSONObject("oferta").getInt("reserva_count");
                        int habitaciones_restantes = habitaciones_antes-menos;
                        if(habitaciones_restantes>0){
                            vista.setAgotado(0);
                        }else{
                            vista.setAgotado(1);

                        }
                        vista.setNumero_habitaciones(habitaciones_restantes);

                        Log.e("vista",vista.toString());
                        lista.add(vista);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }

        Log.e("respuesta_size",lista.size()+"");
        return lista;
    }
}
